#!/bin/bash
reset

cd src
mkdir -p bin
make
cd ../src

mkdir -p ../results
mkdir -p ../tests
mkdir -p ../tests/summaryperinstance

seeds=( 2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 101 103 107 109 113 127 131 137 139 149 151 157 163 167 173 179 181 191 193 197 199 211 223 227 229 )
instances=( comp01 comp02 comp03 comp04 comp05 comp06 comp07 comp08 comp09 comp10 comp11 comp12 comp13 comp14 comp15 comp16 comp17 comp18 comp19 comp20 comp21 )
#Algoritmos de Construção
testes=( __C1_LM3, __C2_LM3, __C3_LM3)

title="$instance"
for teste in ${testes[@]} ; do
	echo "testing $teste..."
	#Cria o diretório onde vão ficar os resultados
	mkdir -p ../tests/$teste

	#Executa as instâncias para todas as seeds
	for instance in ${instances[@]} ; do
			echo "Instancia $instance"
			instancia=../instances/$instance.ctt

			for seed in ${seeds[@]} ; do
				#echo "Seed $seed"
				resultado=../results/$teste.$instance.$seed.res
				saida=../tests/$teste/$instance-$seed.dat

				#Testing Contruction Algorithms
				if [ $teste = __C1_LM3 ]; then #Wallace
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=10 const=0 grafico=1 > $saida
				elif [ $teste = __C2_LM3 ]; then #GulosaLuHao
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=10 const=1 grafico=1 > $saida
				elif [ $teste = __C3_LM3 ]; then #GulosaComAleatoriedade
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=10 const=2 grafico=1 > $saida

				else
				  echo "Invalid test ($teste)"
				fi

			done
			cat ../tests/$teste/$instance-* > ../tests/summaryperinstance/$teste-$instance.dat
	done
done

echo "done"
