/*
 * GraspReativo.h
 *
 *  Created on: april 02, 2016
 *      Author: renan
 */

#ifndef DISS_VETOR_GRASPREATIVO_H_
#define DISS_VETOR_GRASPREATIVO_H_

#include "Model/Individuo.h"
class Problema;

class GraspReativo {
public:
	static const int v = 6;

	int nMaxIter;
	int nMaxIterSemMelhora;
	int pesoHard;
	int seed;
	float threshold;//Para construcao da solu;'ao inicial
  int mediaSolucoes;
  int soft1, soft2, soft3, soft4;
  int f1, f2, f3;
  int opBuscaLocalGrasp;
  int info;
  int imprimeGrafico;
  int imprimeFO;

	// [grasp reativo]
	int theta;
	int gama;
	double thresholds[v];
	double probability[v];
	double q[v];
	int count[v];
	int score[v];
	double avg[v];
	double alpha;
	double sigma;
	// [end].

  //Parametros da Busca Local
  int nMaxIterBuscaLocal;
  int k;
  int vizinhancasUtilizadas;
  char nomeArquivoInstanciaInicial[30];
  //Parametros do SA
	double temperaturaInicial, temperaturaFinal;
	double taxaDecaimentoTemperatura;
	int aceitaPioraSA;
  double tempoLimite;
  double tempoExecucao;
	int iteracoesExecutadas;
	int nMovimentosRealizados;
	int nMoves, nSwaps, nKempes, nTimeMoves, nRoomMoves, nLectureMoves, nRoomStabilityMoves, nMinWorkingDaysMoves, nCurriculumCompactnessMoves;
	int nMovesValidos, nSwapsValidos, nKempesValidos, nTimeMovesValidos, nRoomMovesValidos, nLectureMovesValidos, nRoomStabilityMovesValidos, nMinWorkingDaysMovesValidos, nCurriculumCompactnessMovesValidos;
	int nMovesMelhora, nSwapsMelhora, nKempesMelhora, nTimeMovesMelhora, nRoomMovesMelhora, nLectureMovesMelhora, nRoomStabilityMovesMelhora, nMinWorkingDaysMovesMelhora, nCurriculumCompactnessMovesMelhora;
	int nMovesInvalido, nSwapsInvalido, nKempesInvalido, nTimeMovesInvalido, nRoomMovesInvalido, nLectureMovesInvalido, nRoomStabilityMovesInvalido, nMinWorkingDaysMovesInvalido, nCurriculumCompactnessMovesInvalido;

	GraspReativo();

	Individuo* executa(Problema* p);
	void ImprimeExecucao();

private:
	Individuo* executaHillClimbing(Problema* p, Individuo* indInicial);
	Individuo* executaSimulatedAnnealing(Problema* p, Individuo* indInicial);
	int usouTodosAlphas();
};

#endif /* DISS_VETOR_GRASPREATIVO_H_ */
