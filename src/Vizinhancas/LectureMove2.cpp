/*
 * LectureMove2.cpp
 *
 *  Created on: Feb 16, 2016
 *      Author: renan
 */

#include "LectureMove2.h"
#include "../Model/Alocacao.h"
#include "TimeMove.h"

LectureMove2::LectureMove2(Problema *pro, Individuo* piInd){
 	ind = piInd;
	p = pro;
	/* Escolhe randomicamente uma aula alocada */
	pos1 = rand() % ind->aulasAlocadas.size();
	a1 = ind->aulasAlocadas[pos1];

	/* Monta a lista de horários disponíveis */
	list<Alocacao*> horariosPossiveis;
	for(vector<Alocacao*>::iterator it = ind->TodosHorarios.begin(); it != ind->TodosHorarios.end(); it++)
	{
		if(!p->HorarioIndisponivelDisciplina(a1->aula->disciplina, (*(it))->horario->horario) &&
			 ((*(it))->aula == NULL || (a1->aula->disciplina->numeroSequencial != (*(it))->aula->disciplina->numeroSequencial)))
		{
				horariosPossiveis.push_front(*it);
		}
	}

	/* Escolhe um horário aleatório dentre os disponíveis */
	int posAula = rand() % horariosPossiveis.size();
	list<Alocacao*>::iterator it = horariosPossiveis.begin();
	/* Avança até a posição escolhida */
	advance(it, posAula);
	a2 = *(it);

	pos2 = -1;
	if (a2->aula == NULL){
		for( int i=0; i<(int)ind->horariosVazios.size(); i++){
			if( ind->horariosVazios[i]->id == a2->id ){
				pos2 = i;
				break;
			}
		}
		m = new Move(ind, pos1, pos2);
	}
	else {
		for( int i=0; i<(int)ind->aulasAlocadas.size(); i++){
			if( ind->aulasAlocadas[i]->id == a2->id ){
				pos2 = i;
				break;
			}
		}
		m = new Swap(ind, pos1, pos2);
	}

  if( m->deltaFit > 0 ){
    /*printf("A1 escolhida pelo LectureMove2:");
    a1->imprime();
    printf("A2 escolhida pelo LectureMove2:");
    a2->imprime();*/

    delete(m); // Deleta o movimento anterior e tenta fazer um TimeMove.
	  m = new TimeMove(ind, pos1);
  }

    deltaFit = m->deltaFit;
 }
 
 
LectureMove2::LectureMove2(Problema *pro, Individuo* piInd, int posAulaAlocada, int posTodosHorarios){
 	ind = piInd;
	p = pro;
	/* Escolhe randomicamente uma aula alocada */
	pos1 = posAulaAlocada;
	a1   = ind->aulasAlocadas[pos1];

	a2 = ind->TodosHorarios[posTodosHorarios];

	pos2 = -1;
	if (a2->aula == NULL){
		for( int i=0; i<(int)ind->horariosVazios.size(); i++){
			if( ind->horariosVazios[i]->id == a2->id ){
				pos2 = i;
				break;
			}
		}
		m = new Move(ind, pos1, pos2);
	}
	else {
		for( int i=0; i<(int)ind->aulasAlocadas.size(); i++){
			if( ind->aulasAlocadas[i]->id == a2->id ){
				pos2 = i;
				break;
			}
		}
		m = new Swap(ind, pos1, pos2);
	}

  if( m->deltaFit > 0 ){
    /*printf("A1 escolhida pelo LectureMove2:");
    a1->imprime();
    printf("A2 escolhida pelo LectureMove2:");
    a2->imprime();*/

    delete(m); // Deleta o movimento anterior e tenta fazer um TimeMove.
	  m = new TimeMove(ind, pos1);
  }

    deltaFit = m->deltaFit;
 }

 void LectureMove2::aplicaMovimento(){
	if (m->tipoMovimento == 1) {
		/* Remove A1 da lista de aulas alocadas */
		ind->aulasAlocadas.erase(ind->aulasAlocadas.begin()+pos1);

		/* Remove a2 da lista de alocações vazias */
		ind->horariosVazios.erase(ind->horariosVazios.begin()+pos2);

		/* Aplica o Movimento */
		m->aplicaMovimento();

		/* Insere A1 na lista de aulas alocadas */
		ind->aulasAlocadas.push_back(a1);

		/* Insere A2 na lista de alocações vazias, de modo ordenado */
		vector<Alocacao*>::iterator it;
		for(it = ind->horariosVazios.begin(); it != ind->horariosVazios.end() && (*it)->sala->numeroSequencial <= a2->sala->numeroSequencial; it++) {
			if ( (*it)->sala->numeroSequencial == a2->sala->numeroSequencial && (*it)->horario->horario > a2->horario->horario ) {
			 break;
			}
		}
		ind->horariosVazios.insert (it, a2);
	} else {
		m->aplicaMovimento();
	}
 }

 LectureMove2::~LectureMove2(){
   delete(m);
 }
