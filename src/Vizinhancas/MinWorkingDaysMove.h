/*
 * RoomStabilityMove.h
 *
 *  Created on: Feb 12, 2016
 *      Author: renan
 */

#ifndef MINWORKINGDAYSMOVE_H_
#define MINWORKINGDAYSMOVE_H_

#include "Movimento.h"
#include "../Model/Individuo.h"
#include "../Model/Problema.h"
#include "../Restricoes/RestricaoFraca2MinimumWorkingDays.h"
#include "Swap.h"
#include "Move.h"

class MinWorkingDaysMove : public Movimento{
	public:
		MinWorkingDaysMove(Problema* p, Individuo* piInd);
		MinWorkingDaysMove(Problema* p, Individuo* piInd, Alocacao* alocacao);
		void aplicaMovimento();
		virtual ~MinWorkingDaysMove();

	private:
		Movimento* m;
		RestricaoFraca2MinimumWorkingDays* restricao;
		void imprimeListaAlocada();
		bool naoPossuiAulaNoDia(int dia);
};

#endif
