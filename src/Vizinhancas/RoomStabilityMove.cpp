/*
 * RoomStabilityMove.cpp
 *
 *  Created on: Feb 11, 2016
 *      Author: renan
 */

#include "RoomStabilityMove.h"
#include "../Model/Alocacao.h"
#include "../Model/Aula.h"
#include "../Model/Disciplina.h"
#include "../Restricoes/RestricaoFraca4RoomStability.h"
#include "Swap.h"
#include "Move.h"

RoomStabilityMove::RoomStabilityMove(Problema* p, Individuo* piInd){

	ind = piInd;
	restricao = new RestricaoFraca4RoomStability();

	vector<Alocacao*>::iterator it, it2;
	Movimento* m;
	Alocacao* alocEscolhida;
	int disciplinaEscolhida, salaEscolhida;
	int deltaFitInicial = ind->fitness;

	if (restricao->contaViolacaoRestricao(ind, p) <= 0){
		deltaFit = 99999; deltaHard  = 0;
		return;
	}

	alocEscolhida       = ind->aulasAlocadas[ rand() % ind->aulasAlocadas.size() ];
	disciplinaEscolhida = alocEscolhida->aula->disciplina->numeroSequencial;
	salaEscolhida       = alocEscolhida->sala->numeroSequencial;

	if (restricao->contaViolacaoRestricao(ind, alocEscolhida->aula->disciplina, p->nSalas) <= 0){
		deltaFit = 99999; deltaHard  = 0;
		return;
	}

	for( pos1 = 0; pos1<(int)ind->aulasAlocadas.size(); pos1++ ) {
		a1 = ind->aulasAlocadas[pos1];

		if( a1->aula->disciplina->numeroSequencial != disciplinaEscolhida ||
			    a1->sala->numeroSequencial == salaEscolhida ) continue;

		for( pos2=0; pos2<(int)ind->aulasAlocadas.size(); pos2++){
			a2 = ind->aulasAlocadas[pos2];

			if( a2->sala->numeroSequencial == salaEscolhida &&
				a2->horario->horario         == a1->horario->horario) {

				m = new Swap(ind, pos1, pos2);
				if(m->deltaFit < 0) {
					movimentos.push_back(m);
					m->aplicaMovimento();
				} else {
					delete(m);
				}
			}
		}
		for( pos2=0; pos2<(int)ind->horariosVazios.size(); pos2++){
			a2 = ind->horariosVazios[pos2];

			if( a2->sala->numeroSequencial == salaEscolhida &&
			    a2->horario->horario       == a1->horario->horario) {

				m = new Move(ind, pos1, pos2);
				if(m->deltaFit < 0) {
					movimentos.push_back(m);
					m->aplicaMovimento();
				} else {
					delete(m);
				}
			}
		}

	}

	deltaFit = ind->fitness - deltaFitInicial;
	desfazMovimentos();

}


RoomStabilityMove::RoomStabilityMove(Problema* p, Individuo* piInd, int posAulaAlocada){

	ind = piInd;
	restricao = new RestricaoFraca4RoomStability();

	vector<Alocacao*>::iterator it, it2;
	Movimento* m;
	Alocacao* alocEscolhida;
	int disciplinaEscolhida, salaEscolhida;
	int deltaFitInicial = ind->fitness;

	if (restricao->contaViolacaoRestricao(ind, p) <= 0){
		deltaFit = 99999; deltaHard  = 0;
		return;
	}

	alocEscolhida       = ind->aulasAlocadas[ posAulaAlocada ];
	disciplinaEscolhida = alocEscolhida->aula->disciplina->numeroSequencial;
	salaEscolhida       = alocEscolhida->sala->numeroSequencial;

	if (restricao->contaViolacaoRestricao(ind, alocEscolhida->aula->disciplina, p->nSalas) <= 0){
		deltaFit = 99999; deltaHard  = 0;
		return;
	}

	for( pos1 = 0; pos1<(int)ind->aulasAlocadas.size(); pos1++ ) {
		a1 = ind->aulasAlocadas[pos1];

		if( a1->aula->disciplina->numeroSequencial == disciplinaEscolhida &&
			a1->sala->numeroSequencial != salaEscolhida) {

			for( pos2=0; pos2<(int)ind->aulasAlocadas.size(); pos2++){
				a2 = ind->aulasAlocadas[pos2];

				if( a2->sala->numeroSequencial == salaEscolhida &&
					a2->horario->dia     == a1->horario->dia &&
					a2->horario->periodo == a1->horario->periodo) {

					m = new Swap(ind, pos1, pos2);
					if(m->deltaFit < 0) {
						movimentos.push_back(m);
						m->aplicaMovimento();
					} else {
						delete(m);
					}
				}
			}
			for( pos2=0; pos2<(int)ind->horariosVazios.size(); pos2++){
				a2 = ind->horariosVazios[pos2];

				if( a2->sala->numeroSequencial == salaEscolhida &&
					a2->horario->dia     == a1->horario->dia &&
					a2->horario->periodo == a1->horario->periodo) {

					m = new Move(ind, pos1, pos2);
					if(m->deltaFit < 0) {
						movimentos.push_back(m);
						m->aplicaMovimento();
					} else {
						delete(m);
					}
				}
			}
		}
	}

	deltaFit = ind->fitness - deltaFitInicial;
	desfazMovimentos();

}

void RoomStabilityMove::desfazMovimentos(){
	list<Movimento*>::iterator itr;

	for(itr = movimentos.begin(); itr != movimentos.end(); itr++){
		(*itr)->desfazMovimento();
	}
}

void RoomStabilityMove::aplicaMovimento() {
	list<Movimento*>::iterator itr;

	for(itr = movimentos.begin(); itr != movimentos.end(); itr++) {
		(*itr)->aplicaMovimento();
		/*
		if ((*itr)->tipoMovimento == 1) {
			// Remove A1 da lista de aulas alocadas
			ind->aulasAlocadas.erase(ind->aulasAlocadas.begin()+(*itr)->pos1);

			// Remove a2 da lista de alocações vazias
			ind->horariosVazios.erase(ind->horariosVazios.begin()+(*itr)->pos2);


			// Aplica o Movimento
			(*itr)->aplicaMovimento();

			// Insere A1 na lista de aulas alocadas
			ind->aulasAlocadas.push_back((*itr)->a1);

			// Insere A2 na lista de alocações vazias, de modo ordenado
			vector<Alocacao*>::iterator it;
			for(it = ind->horariosVazios.begin(); it != ind->horariosVazios.end() && (*it)->sala->numeroSequencial <= (*itr)->a2->sala->numeroSequencial; it++) {
				if ( (*it)->sala->numeroSequencial == (*itr)->a2->sala->numeroSequencial &&
					 (*it)->horario->horario > (*itr)->a2->horario->horario ) {
					break;
				}
			}
			ind->horariosVazios.insert (it, (*itr)->a2);
		} else {
			(*itr)->aplicaMovimento();
		}
		*/
	}
}

RoomStabilityMove::~RoomStabilityMove(){
	Movimento* m;
	while( movimentos.size() > 0 ) {
		m = movimentos.back();
		movimentos.pop_back();
		delete(m);
	}
	delete(restricao);
}
