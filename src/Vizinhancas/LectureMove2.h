/*
 * LectureMove2.h
 *
 *  Created on: Feb 16, 2016
 *      Author: renan
 */


#ifndef LectureMove2_H_
#define LectureMove2_H_

#include "Movimento.h"
#include "Swap.h"
#include "Move.h"
#include "../Model/Individuo.h"
#include "../Model/Problema.h"

class LectureMove2 : public Movimento{
public:
	LectureMove2(Problema* p, Individuo* piInd);
	LectureMove2(Problema *pro, Individuo* piInd, int posAulaAlocada, int posTodosHorarios);
	~LectureMove2();
	void aplicaMovimento();

private:
	Problema* p;
	Movimento* m;
};

#endif /* LectureMove2_H_ */
