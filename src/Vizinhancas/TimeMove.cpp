/*
 * TimeMove.cpp
 *
 *  Created on: Dec 16, 2015
 *      Author: renan
 */

#include "TimeMove.h"
#include "Move.h"
#include "RoomMove.h"
#include "../Model/Alocacao.h"

TimeMove::TimeMove(Individuo* piInd, int posAlocComAula){
	int it;
	tipoMovimento = 4;
	ind = piInd;
	m = NULL;

	pos1 = posAlocComAula;
	a1 = ind->aulasAlocadas[pos1];
    if( a1 == NULL ){
    	fprintf(stderr, "Erro em TimeMove.cpp: Alocação %3d não encontrada no vetor de aulas alocadas\n\n", pos1);
    	exit(0);
    }
    if( a1->aula == NULL ){
    	fprintf(stderr, "Erro em TimeMove.cpp: Alocação de aulas alocadas possui aula nula\n\n");
    	exit(0);
    }

	bool escolheu = false;
	/* Seleciona o primeiro TimeSlot disponível ascendente */
	for(it = 0; it < (int)ind->horariosVazios.size(); it++)
	{
		if( ind->horariosVazios[it]->sala->numeroSequencial == a1->sala->numeroSequencial &&
			ind->horariosVazios[it]->horario->horario > a1->horario->horario)
		{
			pos2 = it;
			a2 = ind->horariosVazios[pos2];
			escolheu = true;
			break;
		}
	}

	if(!escolheu)
	{
		/* Seleciona o primeiro TimeSlot disponível descendente */
		for(it = (int)ind->horariosVazios.size()-1; it >= 0; it--)
		{
			if( ind->horariosVazios[it]->sala->numeroSequencial == a1->sala->numeroSequencial &&
				ind->horariosVazios[it]->horario->horario < a1->horario->horario)
			{
				pos2 = it;
				a2 = ind->horariosVazios[pos2];
				escolheu = true;
				break;
			}
		}
	}

	if(a2 != NULL && escolheu){
		deltaFit = calculaDeltaFitTimeMove(piInd->p);
	} else {
		deltaFit = 99999; deltaHard  = 99999;
	}
}


TimeMove::TimeMove(Problema* p, Individuo* piInd){
	tipoMovimento = 4;
	int it;
	ind = piInd;
	m = NULL;
	/* Escolhe randomicamente uma aula alocada */
	pos1 = rand() % ind->aulasAlocadas.size();
	a1 = ind->aulasAlocadas[pos1];

	bool escolheu = false;

	/* Seleciona o primeiro TimeSlot disponível ascendente */
	for(it = 0; it < (int)ind->horariosVazios.size(); it++)
	{
		if( ind->horariosVazios[it]->sala->numeroSequencial == a1->sala->numeroSequencial &&
			ind->horariosVazios[it]->horario->horario > a1->horario->horario)
		{
			pos2 = it;
			a2 = ind->horariosVazios[pos2];
			escolheu = true;
			break;
		}
	}

	if(!escolheu)
	{
		/* Seleciona o primeiro TimeSlot disponível descendente */
		for(it = (int)ind->horariosVazios.size()-1; it >= 0; it--)
		{
			if( ind->horariosVazios[it]->sala->numeroSequencial == a1->sala->numeroSequencial &&
				ind->horariosVazios[it]->horario->horario < a1->horario->horario)
			{
				pos2 = it;
				a2 = ind->horariosVazios[pos2];
				escolheu = true;
				break;
			}
		}
	}

	if(a2 != NULL && escolheu){
		deltaFit = calculaDeltaFitTimeMove(p);
	} else {
		deltaFit = 99999; deltaHard  = 99999;
	}
}

void TimeMove::aplicaMovimento(){

	if(m == NULL){
		/* Remove A1 da lista de aulas alocadas */
		ind->aulasAlocadas.erase(ind->aulasAlocadas.begin()+pos1);

		/* Remove a2 da lista de alocações vazias */
		ind->horariosVazios.erase(ind->horariosVazios.begin()+pos2);

		aplicaTimeMoveSemRecalculoFuncaoObjetivo();

		ind->fitness += deltaFit;
		ind->hard  += deltaHard;
		ind->soft1 += deltaSoft1;
		ind->soft2 += deltaSoft2;
		ind->soft3 += deltaSoft3;
		ind->soft4 += deltaSoft4;

		/* Insere A1 na lista de aulas alocadas */
		ind->aulasAlocadas.push_back(a1);

		/* Insere A2 na lista de alocações vazias, de modo ordenado */
		vector<Alocacao*>::iterator it;
		for(it = ind->horariosVazios.begin(); it != ind->horariosVazios.end() && (*it)->sala->numeroSequencial <= a2->sala->numeroSequencial; it++) {
			if ( (*it)->sala->numeroSequencial == a2->sala->numeroSequencial && (*it)->horario->horario > a2->horario->horario ) {
				break;
			}
		}
		ind->horariosVazios.insert (it, a2);
	} else {
		m->aplicaMovimento();
	}
}

int TimeMove::calculaDeltaFitTimeMove(Problema* p){
	list<Restricao*>::iterator it;
	int deltaFitness;
	int violaRestricaoHard;

	deltaFitness  = -p->CalculaCustoAulaAlocada(ind, a1, this);

	deltaHard  = 0;
	deltaSoft1 = -deltaSoft1;
	deltaSoft2 = -deltaSoft2;
	deltaSoft3 = -deltaSoft3;
	deltaSoft4 = -deltaSoft4;
	deltaFitness = deltaSoft1 + deltaSoft2 + deltaSoft3 + deltaSoft4;

	aplicaTimeMoveSemRecalculoFuncaoObjetivo();
	violaRestricaoHard = p->violaRestricaoGrave(ind, this);
	if( ! violaRestricaoHard ){
		deltaFitness += p->CalculaCustoAulaAlocada(ind, a1, this);
	}
	else {deltaFitness = 99999; deltaHard  = 99999;}
	aplicaTimeMoveSemRecalculoFuncaoObjetivo();

	return deltaFitness;
}


void TimeMove::aplicaTimeMoveSemRecalculoFuncaoObjetivo(){
	list<Curriculo*>::iterator itCurr;
	Timeslot* temp_hora;
	int aula1;

	aula1 = a1->aula->disciplina->numeroSequencial;
	ind->Alocacao_dias_utilizados[aula1][a1->horario->dia]--;
	ind->Alocacao_salas_utilizadas[aula1][a1->sala->numeroSequencial]--;
	for( itCurr = a1->aula->disciplina->curriculos.begin(); itCurr!=a1->aula->disciplina->curriculos.end(); itCurr++){
		ind->Alocacao_horarios_utilizados_por_curriculo[(*itCurr)->numeroSequencial][a1->horario->horario]--;
		if( ind->Alocacao_horarios_utilizados_por_curriculo[(*itCurr)->numeroSequencial][a1->horario->horario] == 0)
			ind->matrizAlocacaoCurriculoDiasPeriodos[(*itCurr)->numeroSequencial][a1->horario->dia][a1->horario->periodo] = NULL;
	}
	ind->matrizProfessorHorarioQntd[a1->aula->disciplina->professor->numeroSequencial][a1->horario->horario]--;
	if( ind->matrizProfessorHorario[a1->aula->disciplina->professor->numeroSequencial][a1->horario->horario] == a1)
		ind->matrizProfessorHorario[a1->aula->disciplina->professor->numeroSequencial][a1->horario->horario] = NULL;
	//		a1->aula->disciplina->professor->restricaoHorario[a1->horario->horario] = NULL;

	temp_hora = a1->horario;
	a1->horario = a2->horario;
	a2->horario = temp_hora;

	ind->matrizProfessorHorarioQntd[a1->aula->disciplina->professor->numeroSequencial][a1->horario->horario]++;
	if( ind->matrizProfessorHorario[a1->aula->disciplina->professor->numeroSequencial][a1->horario->horario] == NULL)
		ind->matrizProfessorHorario[a1->aula->disciplina->professor->numeroSequencial][a1->horario->horario] = a1;
	//		a1->aula->disciplina->professor->restricaoHorario[a1->horario->horario] = a1;

	ind->Alocacao_dias_utilizados[aula1][a1->horario->dia]++;
	ind->Alocacao_salas_utilizadas[aula1][a1->sala->numeroSequencial]++;
	for( itCurr = a1->aula->disciplina->curriculos.begin(); itCurr!=a1->aula->disciplina->curriculos.end(); itCurr++){
		ind->Alocacao_horarios_utilizados_por_curriculo[(*itCurr)->numeroSequencial][a1->horario->horario]++;
		if( ind->matrizAlocacaoCurriculoDiasPeriodos[(*itCurr)->numeroSequencial][a1->horario->dia][a1->horario->periodo] == NULL )
			ind->matrizAlocacaoCurriculoDiasPeriodos[(*itCurr)->numeroSequencial][a1->horario->dia][a1->horario->periodo] = a1;
	}
}
