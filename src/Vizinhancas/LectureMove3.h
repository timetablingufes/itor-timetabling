/*
 * LectureMove3.h
 *
 *  Created on: Mar 13, 2016
 *      Author: renan
 */


#ifndef LectureMove3_H_
#define LectureMove3_H_

#include "Movimento.h"
#include "Swap.h"
#include "Move.h"
#include "../Model/Individuo.h"
#include "../Model/Problema.h"

class LectureMove3 : public Movimento{
public:
	LectureMove3(Problema* p, Individuo* piInd);
	LectureMove3(Problema *pro, Individuo* piInd, int posAulaAlocada, int posTodosHorarios);
	~LectureMove3();
	void aplicaMovimento();

private:
	int movimento;
	Problema* p;
	Movimento* m;
};

#endif /* LectureMove3_H_ */
