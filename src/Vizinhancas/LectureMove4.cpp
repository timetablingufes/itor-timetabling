/*
 * LectureMove4.cpp
 *
 *  Created on: Mar 16, 2016
 *      Author: renan
 */

#include "LectureMove4.h"
#include "../Model/Alocacao.h"

LectureMove4::LectureMove4(Problema *pro, Individuo* piInd){
 	ind = piInd;
	p = pro;

	int i;

	/* Escolhe randomicamente uma aula alocada */
	pos1 = rand() % ind->aulasAlocadas.size();
	a1 = ind->aulasAlocadas[pos1];

	/* Escolhe um horário aleatório dentre os disponíveis */
	a2 = ind->TodosHorarios[ rand() % ind->TodosHorarios.size() ];

	pos2 = -1;
	if (a2->aula == NULL){
		for( i=0; i<(int)ind->horariosVazios.size(); i++){
			if( ind->horariosVazios[i]->id == a2->id ){
				pos2 = i;
				break;
			}
		}
	    if( pos2 < 0 ){
			for( i=0; i<(int)ind->horariosVazios.size(); i++){
				printf("%3d ", ind->horariosVazios[i]->id);
			}
			printf("\n");
			for( i=0; i<(int)ind->aulasAlocadas.size(); i++)
				printf("%3d ", ind->aulasAlocadas[i]->id);
			printf("\n");
	    	fprintf(stderr, "Erro em LectureMove4.cpp: Alocação %3d não encontrada no vetor de horários vazios\n\n", a2->id);
	    	exit(0);
	    }
	    //printf("m <- Move     (%3d,%3d)\n", a1->id, a2->id);
	    m = new Move(ind, pos1, pos2);
	}
	else {
		for( i=0; i<(int)ind->aulasAlocadas.size(); i++){
			if( ind->aulasAlocadas[i]->id == a2->id ){
				pos2 = i;
				break;
			}
		}
	    if( pos2 < 0 ){
			for( i=0; i<(int)ind->horariosVazios.size(); i++){
				printf("%3d ", ind->horariosVazios[i]->id);
			}
			printf("\n");
			for( i=0; i<(int)ind->aulasAlocadas.size(); i++)
				printf("%3d ", ind->aulasAlocadas[i]->id);
			printf("\n");
	    	fprintf(stderr, "Erro em LectureMove4.cpp: Alocação %3d não encontrada no vetor de aulas alocadas\n\n", a2->id);
	    	exit(0);
	    }
	    //printf("m <- Swap     (%3d,%3d)\n", a1->id, a2->id);
		m = new Swap(ind, pos1, pos2);
	}

	//*
	if( m->deltaFit > 0 ){
		delete(m); // Deleta o movimento anterior e tenta fazer um TimeMove.
		//printf("m <- TimeMove (%3d,%3d)\n", a1->id, a2->id);
		m = new TimeMove(ind, pos1);
	}
	//*/

	deltaFit = m->deltaFit;

 }

LectureMove4::LectureMove4(Problema *pro, Individuo* piInd, int posAulaAlocada, int posTodosHorarios){
 	ind = piInd;
	p = pro;

	int i;

	/* Escolhe randomicamente uma aula alocada */
	pos1 = posAulaAlocada;
	a1   = ind->aulasAlocadas[pos1];

	/* Escolhe um horário aleatório dentre os disponíveis */
	a2   = ind->TodosHorarios[ posTodosHorarios ];

	pos2 = -1;
	if (a2->aula == NULL){
		for( i=0; i<(int)ind->horariosVazios.size(); i++){
			if( ind->horariosVazios[i]->id == a2->id ){
				pos2 = i;
				break;
			}
		}
	    if( pos2 < 0 ){
			for( i=0; i<(int)ind->horariosVazios.size(); i++){
				printf("%3d ", ind->horariosVazios[i]->id);
			}
			printf("\n");
			for( i=0; i<(int)ind->aulasAlocadas.size(); i++)
				printf("%3d ", ind->aulasAlocadas[i]->id);
			printf("\n");
	    	fprintf(stderr, "Erro em LectureMove4.cpp: Alocação %3d não encontrada no vetor de horários vazios\n\n", a2->id);
	    	exit(0);
	    }
	    //printf("m <- Move     (%3d,%3d)\n", a1->id, a2->id);
	    m = new Move(ind, pos1, pos2);
	}
	else {
		for( i=0; i<(int)ind->aulasAlocadas.size(); i++){
			if( ind->aulasAlocadas[i]->id == a2->id ){
				pos2 = i;
				break;
			}
		}
	    if( pos2 < 0 ){
			for( i=0; i<(int)ind->horariosVazios.size(); i++){
				printf("%3d ", ind->horariosVazios[i]->id);
			}
			printf("\n");
			for( i=0; i<(int)ind->aulasAlocadas.size(); i++)
				printf("%3d ", ind->aulasAlocadas[i]->id);
			printf("\n");
	    	fprintf(stderr, "Erro em LectureMove4.cpp: Alocação %3d não encontrada no vetor de aulas alocadas\n\n", a2->id);
	    	exit(0);
	    }
	    //printf("m <- Swap     (%3d,%3d)\n", a1->id, a2->id);
		m = new Swap(ind, pos1, pos2);
	}

	//*
	if( m->deltaFit > 0 ){
		delete(m); // Deleta o movimento anterior e tenta fazer um TimeMove.
		//printf("m <- TimeMove (%3d,%3d)\n", a1->id, a2->id);
		m = new TimeMove(ind, pos1);
	}
	//*/

	deltaFit = m->deltaFit;

 }


void LectureMove4::aplicaMovimento(){

	if (m->tipoMovimento == 1) {
		/* Remove A1 da lista de aulas alocadas */
		ind->aulasAlocadas.erase(ind->aulasAlocadas.begin()+pos1);

		/* Remove a2 da lista de alocações vazias */
		ind->horariosVazios.erase(ind->horariosVazios.begin()+pos2);

		/* Aplica o Movimento */
		m->aplicaMovimento();

		/* Insere A1 na lista de aulas alocadas */
		ind->aulasAlocadas.push_back(a1);

		/* Insere A2 na lista de alocações vazias, de modo ordenado */
		vector<Alocacao*>::iterator it;
		for(it = ind->horariosVazios.begin(); it != ind->horariosVazios.end() && (*it)->sala->numeroSequencial <= a2->sala->numeroSequencial; it++) {
			if ( (*it)->sala->numeroSequencial == a2->sala->numeroSequencial && (*it)->horario->horario > a2->horario->horario ) {
			 break;
			}
		}
		ind->horariosVazios.insert (it, a2);
	} else {
		m->aplicaMovimento();
	}
 }

LectureMove4::~LectureMove4(){
	delete(m);
  }
