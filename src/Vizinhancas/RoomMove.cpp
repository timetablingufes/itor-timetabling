/*
 * RoomMove.cpp
 *
 *  Created on: Dec 26, 2015
 *      Author: renan
 */

#include "RoomMove.h"
#include "Move.h"
#include "../Model/Alocacao.h"
#include <string.h>

RoomMove::RoomMove(Individuo* piInd, Alocacao* al1, int posAlocComAula){
	tipoMovimento = 5;
	int it;
	ind = piInd;

	pos1 = posAlocComAula;
	a1   = ind->aulasAlocadas[pos1];
    if( a1 == NULL ){
    	fprintf(stderr, "Erro em RoomMove.cpp: Alocação %3d não encontrada no vetor de aulas alocadas\n\n", pos1);
    	exit(0);
    }

	bool escolheu = false;
	a2 = NULL;

	/* Escolhe uma possível sala ascendente */
	for(it = 0; it < (int)ind->horariosVazios.size(); it++)
	{
		if( ind->horariosVazios[it]->horario->horario == a1->horario->horario &&
			ind->horariosVazios[it]->sala->numeroSequencial > a1->sala->numeroSequencial )
		{
			pos2 = it;
			a2 = ind->horariosVazios[pos2];
			escolheu = true;
			break;
		}
	}

	if(!escolheu)
	{
		/* Seleciona a primeira sala disponível descendente */
		for(it = (int)ind->horariosVazios.size()-1; it >= 0; it--)
		{
			if( ind->horariosVazios[it]->horario->horario == a1->horario->horario &&
				ind->horariosVazios[it]->sala->numeroSequencial < a1->sala->numeroSequencial)
			{
				pos2 = it;
				a2 = ind->horariosVazios[pos2];
				escolheu = true;
				break;
			}
		}
	}

	if(a2 != NULL && escolheu) {
		deltaFit = calculaDeltaFitRoomMove(piInd->p);
	} else {
		deltaFit = 99999; deltaHard  = 99999;
	}
}


RoomMove::RoomMove(Individuo* piInd, int posAlocComAula){
	tipoMovimento = 5;
	int it;
	ind = piInd;

	pos1 = posAlocComAula;
	a1   = ind->aulasAlocadas[pos1];
    if( a1 == NULL ){
    	fprintf(stderr, "Erro em RoomMove.cpp: Alocação %3d não encontrada no vetor de aulas alocadas\n\n", pos1);
    	exit(0);
    }

	bool escolheu = false;
	a2  = NULL;
	

	/* Escolhe uma possível sala ascendente */
	for(it = 0; it < (int)ind->horariosVazios.size(); it++)
	{
		if( ind->horariosVazios[it]->horario->horario == a1->horario->horario &&
			ind->horariosVazios[it]->sala->numeroSequencial > a1->sala->numeroSequencial )
		{
			pos2 = it;
			a2 = ind->horariosVazios[pos2];
			escolheu = true;
			break;
		}
	}

	if(!escolheu)
	{
		/* Seleciona a primeira sala disponível descendente */
		for(it = (int)ind->horariosVazios.size()-1; it >= 0; it--)
		{
			if( ind->horariosVazios[it]->horario->horario == a1->horario->horario &&
				ind->horariosVazios[it]->sala->numeroSequencial < a1->sala->numeroSequencial)
			{
				pos2 = it;
				a2 = ind->horariosVazios[pos2];
				escolheu = true;
				break;
			}
		}
	}

	if(a2 != NULL && escolheu) {
		deltaFit = calculaDeltaFitRoomMove(piInd->p);
	} else {
		deltaFit = 99999; deltaHard  = 99999;
	}
}


RoomMove::RoomMove(Individuo* piInd, int posAlocComAula, int posTodosHorarios){
	tipoMovimento = 5;
	ind = piInd;

	pos1 = posAlocComAula;
	a1   = ind->aulasAlocadas[pos1];
    if( a1 == NULL ){
    	fprintf(stderr, "Erro em RoomMove.cpp: Alocação %3d não encontrada no vetor de aulas alocadas\n\n", pos1);
    	exit(0);
    }

	/* Escolhe um horário aleatório dentre os disponíveis */
	if( ind->TodosHorarios[posTodosHorarios]->horario->horario       == a1->horario->horario &&
		ind->TodosHorarios[posTodosHorarios]->sala->numeroSequencial != a1->sala->numeroSequencial ) {
		a2 = ind->TodosHorarios[ posTodosHorarios ];
	}
	else{
		deltaFit = 99999; deltaHard  = 99999;
		return;
	}

	pos2 = -1;
	if (a2->aula == NULL){
		for( int i=0; i<(int)ind->horariosVazios.size(); i++){
			if( ind->horariosVazios[i]->id == a2->id ){
				pos2 = i;
				a2   = ind->horariosVazios[pos2];
				break;
			}
		}
	}
	else {
		for( int i=0; i<(int)ind->aulasAlocadas.size(); i++){
			if( ind->aulasAlocadas[i]->id == a2->id ){
				pos2 = i;
				a2   = ind->aulasAlocadas[pos2];
				break;
			}
		}
	}

	if(pos2 >= 0) {
		deltaFit = calculaDeltaFitRoomMove(piInd->p);
	} else {
		deltaFit = 99999; deltaHard  = 99999;
	}
}

RoomMove::RoomMove(Problema* p, Individuo* piInd){
	tipoMovimento = 5;
	int it;
	ind = piInd;

	/* Escolhe randomicamente uma aula alocada */
	pos1 = rand() % ind->aulasAlocadas.size();
	a1 = ind->aulasAlocadas[pos1];

	bool escolheu = false;
	a2 = NULL;

	/* Escolhe uma possível sala ascendente */
	for(it = 0; it < (int)ind->horariosVazios.size(); it++)
	{
		if( ind->horariosVazios[it]->horario->horario == a1->horario->horario &&
			ind->horariosVazios[it]->sala->numeroSequencial > a1->sala->numeroSequencial )
		{
			pos2 = it;
			a2 = ind->horariosVazios[pos2];
			escolheu = true;
			break;
		}
	}

	if(!escolheu)
	{
		/* Seleciona a primeira sala disponível descendente */
		for(it = (int)ind->horariosVazios.size()-1; it >= 0; it--)
		{
			if( ind->horariosVazios[it]->horario->horario == a1->horario->horario &&
				ind->horariosVazios[it]->sala->numeroSequencial < a1->sala->numeroSequencial )
			{
				pos2 = it;
				a2 = ind->horariosVazios[pos2];
				escolheu = true;
				break;
			}
		}
	}

	if(a2 != NULL && escolheu){
		deltaFit = calculaDeltaFitRoomMove(p);
	} else {
		deltaFit = 99999; deltaHard  = 99999;
	}
}

void RoomMove::aplicaMovimento(){

	aplicaRoomMoveSemRecalculoFuncaoObjetivo();

	ind->fitness += deltaFit;
	ind->hard  += deltaHard;
	ind->soft1 += deltaSoft1;
	ind->soft2 += deltaSoft2;
	ind->soft3 += deltaSoft3;
	ind->soft4 += deltaSoft4;
	
}


int RoomMove::calculaDeltaFitRoomMove(Problema* p) {
	list<Restricao*>::iterator it;
	int deltaFitness;
	int violaRestricaoHard;

	deltaFitness  = -p->CalculaCustoAulaAlocada(ind, a1, this);//alimenta os deltaSofts
	if( pos2>=0 ) deltaFitness -=  p->CalculaCustoAulaAlocada(ind, a2, this);//alimenta os deltaSofts

	deltaHard  = 0;
	deltaSoft1 = -deltaSoft1;
	deltaSoft2 = -deltaSoft2;
	deltaSoft3 = -deltaSoft3;
	deltaSoft4 = -deltaSoft4;
	deltaFitness = deltaSoft1 + deltaSoft2 + deltaSoft3 + deltaSoft4;

	aplicaRoomMoveSemRecalculoFuncaoObjetivo();
	violaRestricaoHard = p->violaRestricaoGrave(ind, this);
	if( ! violaRestricaoHard ){
		deltaFitness += p->CalculaCustoAulaAlocada(ind, a1, this);
		if( pos2>=0 ) deltaFitness += p->CalculaCustoAulaAlocada(ind, a2, this);
	}
	else {deltaFitness = 99999; deltaHard  = 99999;}
	aplicaRoomMoveSemRecalculoFuncaoObjetivo();

	return deltaFitness;
}


void RoomMove::aplicaRoomMoveSemRecalculoFuncaoObjetivo(){
	list<Curriculo*>::iterator itCurr;
	Sala* temp_sala;
	int aula1;
	int aula2;

	aula1 = a1->aula->disciplina->numeroSequencial;
	aula2 = (a2->aula != NULL) ? a2->aula->disciplina->numeroSequencial : -1;	
	
	ind->Alocacao_salas_utilizadas[aula1][a1->sala->numeroSequencial]--;
	if( aula2 >= 0 ) {
		ind->Alocacao_salas_utilizadas[aula2][a2->sala->numeroSequencial]--;
	}

	temp_sala = a1->sala;
	a1->sala = a2->sala;
	a2->sala = temp_sala;

	ind->Alocacao_salas_utilizadas[aula1][a1->sala->numeroSequencial]++;
	if( aula2 >= 0 ) {
		ind->Alocacao_salas_utilizadas[aula2][a2->sala->numeroSequencial]++;
	}
	
}
