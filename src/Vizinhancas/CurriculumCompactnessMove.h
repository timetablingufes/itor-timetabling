#ifndef CurriculumCompactnessMove_H_
#define CurriculumCompactnessMove_H_

#include "Movimento.h"
#include "../Model/Individuo.h"
#include "../Model/Problema.h"
#include "../Restricoes/RestricaoFraca3CurriculumCompactness.h"

class CurriculumCompactnessMove : public Movimento {
  public:
    CurriculumCompactnessMove(Problema *p, Individuo* piInd);
    void aplicaMovimento();
    virtual ~CurriculumCompactnessMove();

  private:
    RestricaoFraca3CurriculumCompactness* restricao;
    void imprimeListaAlocada();

};

#endif
