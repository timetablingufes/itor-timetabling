/*
 * LectureMove.h
 *
 *  Created on: Aug 16, 2015
 *      Author: renan
 */


#ifndef LectureMove_H_
#define LectureMove_H_

#include "Movimento.h"
#include "Swap.h"
#include "Move.h"
#include "../Model/Individuo.h"
#include "../Model/Problema.h"

class LectureMove : public Movimento{
public:
	LectureMove(Problema* p, Individuo* piInd);
	LectureMove(Problema *pro, Individuo* piInd, int posAulaAlocada, int posTodosHorarios);
	void aplicaMovimento();
	~LectureMove();

private:
	Problema* p;
	Movimento* m;
};

#endif /* LectureMove_H_ */
