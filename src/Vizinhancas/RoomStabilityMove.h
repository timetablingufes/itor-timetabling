/*
 * RoomStabilityMove.h
 *
 *  Created on: Feb 11, 2016
 *      Author: renan
 */

#ifndef ROOMSTABILITYMOVE_H_
#define ROOMSTABILITYMOVE_H_

#include "Movimento.h"
#include "../Model/Individuo.h"
#include "../Model/Problema.h"

class RoomStabilityMove : public Movimento{
public:
	RoomStabilityMove(Problema* p, Individuo* piInd);
	RoomStabilityMove(Problema* p, Individuo* piInd, int posAulaAlocada);
	void aplicaMovimento();
	virtual ~RoomStabilityMove();
private:
	RestricaoFraca4RoomStability *restricao;
	list<Movimento*> movimentos;
	int codMovimento;
	void desfazMovimentos();
	int calculaDeltaFitRoomStabilityMove();
};

#endif
