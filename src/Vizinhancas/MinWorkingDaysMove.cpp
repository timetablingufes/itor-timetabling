
#include "MinWorkingDaysMove.h"
#include "../Model/Alocacao.h"
#include "../Model/Aula.h"
#include "../Model/Disciplina.h"

class opAloc{
public:
	Alocacao* a;
	int posLista;

	opAloc(Alocacao* piA, int piPos){
		a = piA;
		posLista = piPos;
	};
	~opAloc(){ };
};

bool MinWorkingDaysMove::naoPossuiAulaNoDia(int dia)
{
	if(ind->Alocacao_dias_utilizados[a1->aula->disciplina->numeroSequencial][dia] == 0){
		return true;
	} else {
		return false;
	}
}

MinWorkingDaysMove::MinWorkingDaysMove(Problema* p, Individuo* piInd)
{
	ind = piInd;
	int i;
	restricao = new RestricaoFraca2MinimumWorkingDays();

	// verifica se alguma disciplina da solução viola a restrição
	if (restricao->contaViolacaoRestricao(ind, p) <= 0){
		deltaFit = 99999; deltaHard  = 0;
		return;
	}

	// escolhe uma aula aleatória entre as aulas aulasAlocadas
	pos1 = rand() % ind->aulasAlocadas.size();
	a1   = ind->aulasAlocadas[pos1];
	pos2 = -1;
	a2   = NULL;

	// verifica se a disciplina viola a restrição
	if(restricao->contaViolacaoRestricao(ind, a1->aula->disciplina, p->nDias) <= 0){
		deltaFit = 99999; deltaHard  = 0;
		return;
	}

	// se naquele dia existem duas aulas da mesma disciplina
	if(ind->Alocacao_dias_utilizados[a1->aula->disciplina->numeroSequencial][a1->horario->dia] <= 1){
		deltaFit = 99999; deltaHard = 0;
		return;
	}

	/* Monta a lista de horários disponíveis */
	vector<opAloc*> horariosPossiveis;
	horariosPossiveis.reserve(p->nAulasTotal);

	for(i = 0; i < (int)ind->TodosHorarios.size(); i++){
		if( !p->HorarioIndisponivelDisciplina(a1->aula->disciplina, ind->TodosHorarios[i]->horario->horario) &&
			naoPossuiAulaNoDia(ind->TodosHorarios[i]->horario->dia)){
				horariosPossiveis.push_back(new opAloc(ind->TodosHorarios[i], 0));
		}
	}

	if( horariosPossiveis.size() <= 0 ){
		deltaFit = 99999; deltaHard = 0;
		return;
	}

	/* Escolhe um horário aleatório dentre os disponíveis */
	int posEscolhida = rand() % horariosPossiveis.size();
	a2   = horariosPossiveis[posEscolhida]->a;

	if (a2->aula == NULL){
		for(i = 0; i < (int)ind->horariosVazios.size(); i++){
			if( ind->horariosVazios[i]->id == a2->id ){
				pos2 = i;
			}
		}
		m = new Move(ind, pos1, pos2);
	}
	else {
		for(i = 0; i < (int)ind->aulasAlocadas.size(); i++){
			if( ind->aulasAlocadas[i]->id == a2->id ){
				pos2 = i;
			}
		}
		m = new Swap(ind, pos1, pos2);
	}

	deltaFit = m->deltaFit;


}

MinWorkingDaysMove::MinWorkingDaysMove(Problema* p, Individuo* piInd, Alocacao* alocacao)
{
	int i;
	ind = piInd;
	restricao = new RestricaoFraca2MinimumWorkingDays();

	pos1 = -1;
	pos2 = -1;
	// verifica se alguma disciplina da solução viola a restrição
	if (restricao->contaViolacaoRestricao(ind, p) <= 0){
		deltaFit = 99999; deltaHard = 0;
		return;
	}

	// escolhe uma aula aleatória entre as aulas aulasAlocadas
	a1 = alocacao;
	for(i = 0; i < (int)ind->aulasAlocadas.size(); i++){
		if( a1->id == ind->aulasAlocadas[i]->id ){
			pos1 = i;
			break;
		}
	}

	// verifica se a disciplina viola a restrição
	if(restricao->contaViolacaoRestricao(ind, a1->aula->disciplina, p->nDias) <= 0){
		deltaFit = 99999; deltaHard = 0;
		return;

	}

	// verifica se existem duas aulas da disciplina escolhida no mesmo dia
	if(ind->Alocacao_dias_utilizados[a1->aula->disciplina->numeroSequencial][a1->horario->dia] <= 1){
		deltaFit = 99999; deltaHard = 0;
		return;
	}

	/* Monta a lista de horários disponíveis */
	vector<opAloc*> horariosPossiveis;
	horariosPossiveis.reserve(p->nAulasTotal);
	for(i = 0; i < (int)ind->horariosVazios.size(); i++){
		if( !p->HorarioIndisponivelDisciplina(a1->aula->disciplina, ind->horariosVazios[i]->horario->horario) &&
			naoPossuiAulaNoDia(ind->horariosVazios[i]->horario->dia)){
				horariosPossiveis.push_back(new opAloc(ind->horariosVazios[i], i));
		}
	}
	for(i = 0; i < (int)ind->aulasAlocadas.size(); i++){
		if( !p->HorarioIndisponivelDisciplina(a1->aula->disciplina, ind->aulasAlocadas[i]->horario->horario) &&
			naoPossuiAulaNoDia(ind->aulasAlocadas[i]->horario->dia)){
				horariosPossiveis.push_back(new opAloc(ind->aulasAlocadas[i], i));
		}

	}

	if( horariosPossiveis.size() <= 0 ){
		deltaFit = 99999; deltaHard = 0;
		return;
	}
	/* Escolhe um horário aleatório dentre os disponíveis */
	int posEscolhida = rand() % horariosPossiveis.size();
	a2   = horariosPossiveis[posEscolhida]->a;
	pos2 = horariosPossiveis[posEscolhida]->posLista;

	if (a2->aula == NULL)
		m = new Move(ind, pos1, pos2);
	else
		m = new Swap(ind, pos1, pos2);

	deltaFit = m->deltaFit;

}

MinWorkingDaysMove::~MinWorkingDaysMove()
{
  delete(restricao);
}

void MinWorkingDaysMove::aplicaMovimento()
{
	if (m->tipoMovimento == 1) {
		/* Remove A1 da lista de aulas alocadas */
		ind->aulasAlocadas.erase(ind->aulasAlocadas.begin()+m->pos1);

		/* Remove a2 da lista de alocações vazias */
		ind->horariosVazios.erase(ind->horariosVazios.begin()+m->pos2);

		/* Aplica o Movimento */
		m->aplicaMovimento();

		/* Insere A1 na lista de aulas alocadas */
		ind->aulasAlocadas.push_back(m->a1);

		/* Insere A2 na lista de alocações vazias, de modo ordenado */
		vector<Alocacao*>::iterator it;
		for(it = ind->horariosVazios.begin(); it != ind->horariosVazios.end() && (*it)->sala->numeroSequencial <= m->a2->sala->numeroSequencial; it++) {
			if ( (*it)->sala->numeroSequencial == m->a2->sala->numeroSequencial && (*it)->horario->horario > m->a2->horario->horario ) {
				break;
			}
		}
		ind->horariosVazios.insert (it, m->a2);
	} else {
		m->aplicaMovimento();
	}
}
