/*
 * GraspReativoReativo.cpp
 *
 *  Created on: april 02, 2016
 *      Author: renan
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>
#include "GraspReativo.h"
#include "GeraSolucacoInicial.h"
#include "PathRelinking.h"
#include "Model/Problema.h"
#include "BuscaLocal/HillClimbing.h"
#include "BuscaLocal/SimulatedAnnealing.h"

GraspReativo::GraspReativo() {
	pesoHard = 10000;
	vizinhancasUtilizadas = 0;//move 50% + swap 50%
	//Construcao GraspReativo
  threshold = 0.15;
	//HC
	nMaxIter = 999;
	nMaxIterSemMelhora = INT_MAX;
	nMaxIterBuscaLocal = 150000;
	k = 10;
	//SA
	temperaturaInicial = 1.5;
	temperaturaFinal   = 0.005;
	taxaDecaimentoTemperatura = 0.999;
	aceitaPioraSA = 1;

  mediaSolucoes = 0;
  soft1 = soft2 = soft3 = soft4 = 0;
  f1 = f2 = f3 = 0;
  opBuscaLocalGrasp = 2;//busca padrao = HC
  info = 0;
  imprimeGrafico = 0;
  imprimeFO = 0;
  iteracoesExecutadas = 0;
  tempoExecucao = 0;
  tempoLimite = 999999999;
  seed = time(NULL);
  nomeArquivoInstanciaInicial[0] = '\0';
  nMovimentosRealizados = 0;

	// [grasp reativo]
	gama = 5;
	theta = 10;
	alpha = 0.0;

	thresholds[0] = 0.0;
	thresholds[1] = 0.1;
	thresholds[2] = 0.2;
	thresholds[3] = 0.3;
	thresholds[4] = 0.4;
	thresholds[5] = 0.5;

	for(int i=0; i < v; i++){
		count[i] = 0;
		score[i] = 0;
		avg[i] = 0;
		probability[i] = 1/(double)v;
		q[i] = 0;
		sigma = 0;
	}
  // [end].

	nMoves = nSwaps = nKempes = nTimeMoves = nRoomMoves = nLectureMoves = nRoomStabilityMoves = nMinWorkingDaysMoves = nCurriculumCompactnessMoves = 0;
	nMovesValidos = nSwapsValidos = nKempesValidos = nTimeMovesValidos = nRoomMovesValidos = nLectureMovesValidos = nRoomStabilityMovesValidos = nMinWorkingDaysMovesValidos = nCurriculumCompactnessMovesValidos = 0;
	nMovesMelhora = nSwapsMelhora = nKempesMelhora = nTimeMovesMelhora = nRoomMovesMelhora = nLectureMovesMelhora = nRoomStabilityMovesMelhora = nMinWorkingDaysMovesMelhora = nCurriculumCompactnessMovesMelhora = 0;
	nMovesInvalido = nSwapsInvalido = nKempesInvalido = nTimeMovesInvalido = nRoomMovesInvalido = nLectureMovesInvalido = nRoomStabilityMovesInvalido = nMinWorkingDaysMovesInvalido = nCurriculumCompactnessMovesInvalido = 0;
}

int GraspReativo::usouTodosAlphas(){
	for(int i=0; i < v; i++){
		if(count[i] == 0){
			return 0;
		}
	}

	return 1;
}

Individuo* GraspReativo::executa(Problema* p){
	int i, fezPR, pos;
	double c, melhorAlpha;
	Individuo *ind, *bestInd = NULL, *indPr, *indConstrucao;
	GeraSolucacoInicial* geraSolucao;
	PathRelinking* pathrelinking;
	list<Alocacao*>::iterator itAulasAlocadas;
	float tempo_inicial, tempo_construcao, tempo_busca_local, tempo_path_relinking;

	pathrelinking = new PathRelinking();

	tempo_inicial = clock() / CLOCKS_PER_SEC;
	tempoExecucao = tempo_inicial;

	// agora começa de 1.. para o gama dar certo no mod
	for (i = 1; i <= nMaxIter; i++) {
		// [modificações grasp reativo]
		alpha = ((rand() % 10) + 1) / double(10);
		c = 0.0;
		for(pos=0; pos < v && c < alpha; pos++){
			c += probability[pos];
		}
		pos--;
		// [end].

		//srand(seed*20 + i);
		//CONSTRUCAO
  	indConstrucao = new Individuo(p);
  	if( i==1 && nomeArquivoInstanciaInicial[0]!='\0'){
  		if (info) printf("\tLe Solucao Inicial do arquivo '%s'\n", nomeArquivoInstanciaInicial);
  		indConstrucao->LeDoArquivo(nomeArquivoInstanciaInicial);
  	}else{
  		geraSolucao = new GeraSolucacoInicial(p, thresholds[pos]);
			geraSolucao->CriaIndividuoInicial(p, indConstrucao);
	    delete(geraSolucao);
  	}

		p->CalculaFuncaoObjetivo(indConstrucao);
		f1 += indConstrucao->fitness;

		if( indConstrucao->fitness >= 10000 ){
			fprintf(stderr,"ERRO Construcao GraspReativo: Restricao Forte Conflitante\n");
			return indConstrucao;
		}

		if( imprimeGrafico ) printf("%d\t%d\t", i, indConstrucao->fitness);

		if (info) {
			tempo_construcao = clock() / CLOCKS_PER_SEC;
			printf("\tCONSTRUCAO:  ;%d;;%f;;\n", indConstrucao->fitness, tempo_construcao - tempo_inicial);
		}

		//BUSCA LOCAL
    if (opBuscaLocalGrasp == 2) {
    	printf("\n");
    	//ind = executaHillClimbing(p, indConstrucao);
    } else if (opBuscaLocalGrasp == 6) {
    	ind = executaSimulatedAnnealing(p, indConstrucao);
    } else if (opBuscaLocalGrasp == 3) {
    	//ind = executaHillClimbing(p, indConstrucao);
    	ind = executaSimulatedAnnealing(p, ind);
    } else {
      fprintf(stderr,"Tipo de busca local nao foi informada!\n");
      exit(1);
    }


    f2 += ind->fitness;

		if( ind->fitness >= 10000 ){
			fprintf(stderr, "ERRO Busca local: Restricao Forte Conflitantes: %d\n", ind->hard);
			exit(0);
		}

		if( imprimeGrafico ) printf("%d\t", ind->fitness);

    if (info) {
      tempo_busca_local = (clock() / CLOCKS_PER_SEC);
      printf("\tBUSCA LOCAL: ;;%d;;%f;\n", ind->fitness, tempo_busca_local - tempo_inicial);
    }

    soft1 += ind->soft1;
    soft2 += ind->soft2;
    soft3 += ind->soft3;
    soft4 += ind->soft4;

    //PATH RELINKING
    fezPR = 0; // flag: fez Path-Relinking
    if ( i > 2 ) {// se pool de elites ja esta cheio
    	indPr = pathrelinking->executa(ind);
      p->CalculaFuncaoObjetivo(indPr);
      fezPR = 1; // fez Path-Relinking
    }

    if (fezPR) {// se fez Path-Relinking
      if (info){
        tempo_path_relinking = (clock() / CLOCKS_PER_SEC);
      	printf("\tPATH RELINKING: ;%d;%f;s\n", indPr->fitness, tempo_path_relinking - tempo_inicial);
      	printf("\t**********************************\n");
      }
      if (indPr->fitness < ind->fitness) {
      	delete(ind);
        ind = indPr;
      }
      else delete(indPr);
    }

    f3 += ind->fitness;

		if( imprimeGrafico ) printf("%d\t", ind->fitness);
    pathrelinking->atualizaPool(ind);
    mediaSolucoes += ind->fitness;


		// [modificações grasp reativo]
		count[pos]++;
		score[pos] += ind->fitness;

		//DELETE
		if ( (bestInd == NULL) || (ind->fitness < bestInd->fitness) ) {
			if( bestInd != NULL ) delete(bestInd);
			bestInd = ind;
			melhorAlpha = thresholds[pos];
		}
		else delete(ind);

		if ((i % gama) == 0 && usouTodosAlphas()){
			sigma = 0;

			for(int j=0; j < v; j++){
					avg[j] = score[j] / count[j];
					q[j] = pow(bestInd->fitness/avg[j], theta);
			}

			for(int j=0; j < v; j++){
				sigma += q[j];
			}

			for(int j=0; j < v; j++){
				probability[j] = q[j] / sigma;
			}
		}
		// [end].


    if (info) {
				// printf("Iter:%d,FO=%d\n", i + 1, bestInd->fitness);
        printf("\n");
    }

  	//FIM ITERACAO

  	iteracoesExecutadas++;

		if( imprimeGrafico ) printf("%lf\n", (clock()/CLOCKS_PER_SEC)-tempoExecucao);

  	tempoExecucao = (clock()/CLOCKS_PER_SEC) - tempo_inicial;

    if (this->tempoLimite <= tempoExecucao)					break;
  }

	printf("FO: %d    Best Alfa:%lf\n", bestInd->fitness, melhorAlpha);
	for(int i=0; i < v; i++){
		printf("%.2lf probability: %lf count: %d\n", thresholds[i], probability[i], count[i]);
	}
	printf("\n\n");

//	tempoExecucao = (clock()/CLOCKS_PER_SEC) - tempo_inicial;
	if( info ){
		printf("\tITERAÇÔES: %d    - %f s\n", iteracoesExecutadas, tempoExecucao);
		printf("\tNUM MOVIMENTOS REALIZADOS: %d\n", nMovimentosRealizados);
	}

	delete(pathrelinking);

  mediaSolucoes /= iteracoesExecutadas;

  soft1 /= iteracoesExecutadas;
  soft2 /= iteracoesExecutadas;
  soft3 /= iteracoesExecutadas;
  soft4 /= iteracoesExecutadas;

  f1 /= iteracoesExecutadas;
  f2 /= iteracoesExecutadas;
  f3 /= iteracoesExecutadas;

	return bestInd;
}



Individuo* GraspReativo::executaHillClimbing(Problema* p, Individuo* indInicial){
	HillClimbing* hillClimbing;
	Individuo* ind;

	hillClimbing = new HillClimbing(p, nMaxIterBuscaLocal, k);
	ind = hillClimbing->executa(indInicial, vizinhancasUtilizadas, nMaxIterSemMelhora);
	nMovimentosRealizados += hillClimbing->nMovimentosRealizados;
	nMoves     += hillClimbing->nMoves;
	nSwaps     += hillClimbing->nSwaps;
	nKempes    += hillClimbing->nKempes;
	nTimeMoves += hillClimbing->nTimeMoves;
    nRoomMoves += hillClimbing->nRoomMoves;
    nLectureMoves += hillClimbing->nLectureMoves;
	nRoomStabilityMoves += hillClimbing->nRoomStabilityMoves;
	nMinWorkingDaysMoves += hillClimbing->nMinWorkingDaysMoves;
	nCurriculumCompactnessMoves += hillClimbing->nCurriculumCompactnessMoves;
	nMovesMelhora  	   += hillClimbing->nMovesMelhora;
	nSwapsMelhora  	   += hillClimbing->nSwapsMelhora;
	nKempesMelhora 	   += hillClimbing->nKempesMelhora;
	nTimeMovesMelhora  += hillClimbing->nTimeMovesMelhora;
    nRoomMovesMelhora  += hillClimbing->nRoomMovesMelhora;
    nLectureMovesMelhora += hillClimbing->nLectureMovesMelhora;
	nRoomStabilityMovesMelhora += hillClimbing->nRoomStabilityMovesMelhora;
	nMinWorkingDaysMovesMelhora += hillClimbing->nMinWorkingDaysMovesMelhora;
	nCurriculumCompactnessMovesMelhora += hillClimbing->nCurriculumCompactnessMovesMelhora;
	nMovesValidos  	   += hillClimbing->nMovesValidos;
	nSwapsValidos  	   += hillClimbing->nSwapsValidos;
	nKempesValidos 	   += hillClimbing->nKempesValidos;
	nTimeMovesValidos  += hillClimbing->nTimeMovesValidos;
    nRoomMovesValidos  += hillClimbing->nRoomMovesValidos;
    nLectureMovesValidos += hillClimbing->nLectureMovesValidos;
	nRoomStabilityMovesValidos += hillClimbing->nRoomStabilityMovesValidos;
	nMinWorkingDaysMovesValidos += hillClimbing->nMinWorkingDaysMovesValidos;
	nCurriculumCompactnessMovesValidos += hillClimbing->nCurriculumCompactnessMovesValidos;
	nMovesInvalido     += hillClimbing->nMovesInvalido;
	nSwapsInvalido     += hillClimbing->nSwapsInvalido;
	nKempesInvalido    += hillClimbing->nKempesInvalido;
	nTimeMovesInvalido += hillClimbing->nTimeMovesInvalido;
    nRoomMovesInvalido += hillClimbing->nRoomMovesInvalido;
    nLectureMovesInvalido += hillClimbing->nLectureMovesInvalido;
	nRoomStabilityMovesInvalido += hillClimbing->nRoomStabilityMovesInvalido;
	nMinWorkingDaysMovesInvalido += hillClimbing->nMinWorkingDaysMovesInvalido;
	nCurriculumCompactnessMovesInvalido += hillClimbing->nCurriculumCompactnessMovesInvalido;
	delete(hillClimbing);
	return ind;
}

Individuo* GraspReativo::executaSimulatedAnnealing(Problema* p, Individuo* indInicial){
	SimulatedAnnealing* simulatedAnnealing;
	Individuo* ind;

	simulatedAnnealing = new SimulatedAnnealing(p, nMaxIterSemMelhora, temperaturaInicial, temperaturaFinal, taxaDecaimentoTemperatura);
	ind = simulatedAnnealing->executa(indInicial, vizinhancasUtilizadas, info);
	nMovimentosRealizados += simulatedAnnealing->nMovimentosRealizados;
	nMoves     += simulatedAnnealing->nMoves;
	nSwaps     += simulatedAnnealing->nSwaps;
	nKempes    += simulatedAnnealing->nKempes;
	nTimeMoves += simulatedAnnealing->nTimeMoves;
    nRoomMoves += simulatedAnnealing->nRoomMoves;
    nLectureMoves += simulatedAnnealing->nLectureMoves;
	nRoomStabilityMoves += simulatedAnnealing->nRoomStabilityMoves;
	nMinWorkingDaysMoves += simulatedAnnealing->nMinWorkingDaysMoves;
	nCurriculumCompactnessMoves += simulatedAnnealing->nCurriculumCompactnessMoves;
	nMovesMelhora  	   += simulatedAnnealing->nMovesMelhora;
	nSwapsMelhora  	   += simulatedAnnealing->nSwapsMelhora;
	nKempesMelhora 	   += simulatedAnnealing->nKempesMelhora;
	nTimeMovesMelhora  += simulatedAnnealing->nTimeMovesMelhora;
    nRoomMovesMelhora  += simulatedAnnealing->nRoomMovesMelhora;
    nLectureMovesMelhora += simulatedAnnealing->nLectureMovesMelhora;
	nRoomStabilityMovesMelhora += simulatedAnnealing->nRoomStabilityMovesMelhora;
	nMinWorkingDaysMovesMelhora += simulatedAnnealing->nMinWorkingDaysMovesMelhora;
	nCurriculumCompactnessMovesMelhora += simulatedAnnealing->nCurriculumCompactnessMovesMelhora;
	nMovesValidos  	   += simulatedAnnealing->nMovesValidos;
	nSwapsValidos  	   += simulatedAnnealing->nSwapsValidos;
	nKempesValidos 	   += simulatedAnnealing->nKempesValidos;
	nTimeMovesValidos  += simulatedAnnealing->nTimeMovesValidos;
    nRoomMovesValidos  += simulatedAnnealing->nRoomMovesValidos;
    nLectureMovesValidos += simulatedAnnealing->nLectureMovesValidos;
	nRoomStabilityMovesValidos += simulatedAnnealing->nRoomStabilityMovesValidos;
	nMinWorkingDaysMovesValidos += simulatedAnnealing->nMinWorkingDaysMovesValidos;
	nCurriculumCompactnessMovesValidos += simulatedAnnealing->nCurriculumCompactnessMovesValidos;
	nMovesInvalido     += simulatedAnnealing->nMovesInvalido;
	nSwapsInvalido     += simulatedAnnealing->nSwapsInvalido;
	nKempesInvalido    += simulatedAnnealing->nKempesInvalido;
	nTimeMovesInvalido += simulatedAnnealing->nTimeMovesInvalido;
    nRoomMovesInvalido += simulatedAnnealing->nRoomMovesInvalido;
    nLectureMovesInvalido += simulatedAnnealing->nLectureMovesInvalido;
	nRoomStabilityMovesInvalido += simulatedAnnealing->nRoomStabilityMovesInvalido;
	nMinWorkingDaysMovesInvalido += simulatedAnnealing->nMinWorkingDaysMovesInvalido;
	nCurriculumCompactnessMovesInvalido += simulatedAnnealing->nCurriculumCompactnessMovesInvalido;
	delete(simulatedAnnealing);
	return ind;
}


void GraspReativo::ImprimeExecucao(){
	printf("\nSeed:  %d\n", seed);
	printf("Numero maximo de iteracoes:  %d\n", nMaxIter);
//	printf("Numero maximo de iteracoes sem melhora:  %d\n", nMaxIterSemMelhora);

	if( opBuscaLocalGrasp == 2 ) {
		printf("Busca Local: HC\n");
		printf("Maximo de Iteracoes Busca Local sem Melhora: %d\n", nMaxIterSemMelhora);
		printf("Maximo de Iteracoes Busca Local Geral: %d\n", nMaxIterBuscaLocal);
		printf("k:  %d\n", k);
	}
	else if ( opBuscaLocalGrasp == 6 ){
		printf("Busca Local: SA\n");
		printf("Variacao de temperatura:  %lf a %lf, a uma taxa de %lf\n", temperaturaInicial, temperaturaFinal, taxaDecaimentoTemperatura);
		printf("N:  500\n");
	}
	else printf("Busca Local Invalida (%d)\n", opBuscaLocalGrasp);
	printf("Opcao de Vizinhanca:  %d\n", vizinhancasUtilizadas);
	printf("\nNumero de Movimentos Realizados: %d\n", nMovimentosRealizados);
	printf("Total de Movimentos   :  nMv: %5d nSw: %5d nK: %5d nTimeMv: %5d nRoomMv: %5d nLectMv: %5d nRoomStbMv: %5d nMinWkMv: %5d nCurCompMv: %5d\n", nMoves, nSwaps, nKempes, nTimeMoves, nRoomMoves, nLectureMoves, nRoomStabilityMoves, nMinWorkingDaysMoves, nCurriculumCompactnessMoves);
	printf("Movimentos Invalidos  :  nMv: %5d nSw: %5d nK: %5d nTimeMv: %5d nRoomMv: %5d nLectMv: %5d nRoomStbMv: %5d nMinWkMv: %5d nCurCompMv: %5d\n", nMovesInvalido, nSwapsInvalido, nKempesInvalido, nTimeMovesInvalido, nRoomMovesInvalido, nLectureMovesInvalido, nRoomStabilityMovesInvalido, nMinWorkingDaysMovesInvalido, nCurriculumCompactnessMovesInvalido);
	printf("Movimentos Validos    :  nMv: %5d nSw: %5d nK: %5d nTimeMv: %5d nRoomMv: %5d nLectMv: %5d nRoomStbMv: %5d nMinWkMv: %5d nCurCompMv: %5d\n", nMovesValidos, nSwapsValidos, nKempesValidos, nTimeMovesValidos, nRoomMovesValidos, nLectureMovesValidos, nRoomStabilityMovesValidos, nMinWorkingDaysMovesValidos, nCurriculumCompactnessMovesValidos);
	printf("Movimentos com Melhora:  nMv: %5d nSw: %5d nK: %5d nTimeMv: %5d nRoomMv: %5d nLectMv: %5d nRoomStbMv: %5d nMinWkMv: %5d nCurCompMv: %5d", nMovesMelhora, nSwapsMelhora, nKempesMelhora, nTimeMovesMelhora, nRoomMovesMelhora, nLectureMovesMelhora, nRoomStabilityMovesMelhora, nMinWorkingDaysMovesMelhora, nCurriculumCompactnessMovesMelhora);

	printf("\nIteracoes do GraspReativo executadas: %d em %lf s\n", iteracoesExecutadas, tempoExecucao);
	printf("Media das solucoes:  %d\n", mediaSolucoes);
//	printf("\tSoft1:  %d\tSoft2:  %d\tSoft3:  %d\tSoft4:  %d\n", soft1, soft2, soft3, soft4);
//	printf("f1:  %d\tf2:  %d\tf3:  %d\t\n", f1, f2, f3);
}
