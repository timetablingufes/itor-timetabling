/*
 * SDConjugado.h
 *
 *  Created on: 12/12/2016
 *      Author: labotimlocal
 */

#ifndef SDCONJUGADO_H_
#define SDCONJUGADO_H_

#include "BuscaLocal.h"
#include "SteepestDescent.h"

class Problema;
class Individuo;


class SDConjugado : public SteepestDescent {

public:
	SDConjugado(Problema* p);

	Individuo* executa(Individuo* bestInd, const int movimento);

private:
	Individuo* executaSDMoveSwapKERS(Individuo* ind);
	Individuo* executaSDMoveSwapMWD(Individuo* ind);
	Individuo* executaSDMoveSwapRSM(Individuo* ind);
	Individuo* executaSDLectureMove3KERS(Individuo* ind);
	Individuo* executaSDLectureMove3MWD(Individuo* ind);
	Individuo* executaSDLectureMove3RSM(Individuo* ind);
	Individuo* executaSDLectureMove3e4KERS(Individuo* ind);
	Individuo* executaSDLectureMove3e4MWD(Individuo* ind);
	Individuo* executaSDLectureMove3e4RSM(Individuo* ind);
	
	Individuo* executaSDLectureMove3eRoomMoveeTimeMove(Individuo* ind);
	Individuo* executaSDLectureMove3eRoomMove(Individuo* ind);
	Individuo* executaSDLectureMove3eTimeMove(Individuo* ind);
	Individuo* executaSDRoomMoveeTimeMove(Individuo* ind);

};

#endif /* SDCONJUGADO_H_ */
