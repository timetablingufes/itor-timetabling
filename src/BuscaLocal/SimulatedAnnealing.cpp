/*
 * SimulatedAnnealing.cpp
 *
 *  Created on: May 26, 2015
 *      Author: erika
 */

#include <stdlib.h>
#include <math.h>

#include "SimulatedAnnealing.h"
#include "../Model/Problema.h"
#include "../Vizinhancas/Movimento.h"
#include "../Vizinhancas/Move.h"
#include "../Vizinhancas/Swap.h"
#include "../Vizinhancas/TimeMove.h"
#include "../Vizinhancas/TimeMove2.h"
#include "../Vizinhancas/CadeiadeKempe.h"
#include "../Vizinhancas/CadeiadeKempeExtendido.h"
#include "../Vizinhancas/CadeiadeKempeEstendidaRestrSala.h"
#include "../Vizinhancas/RoomMove.h"
#include "../Vizinhancas/RoomMove2.h"
#include "../Vizinhancas/LectureMove.h"
#include "../Vizinhancas/LectureMove2.h"
#include "../Vizinhancas/LectureMove3.h"
#include "../Vizinhancas/LectureMove4.h"
#include "../Vizinhancas/RoomStabilityMove.h"
#include "../Vizinhancas/MinWorkingDaysMove.h"
#include "../Vizinhancas/CurriculumCompactnessMove.h"

const int _MOVE = 1;
const int _SWAP = 2;
const int _KEMPE = 3;
const int _TIMEMOVE = 4;
const int _ROOMMOVE = 5;
const int _LECTUREMOVE = 6;
const int _ROOMSTABILITYMOVE = 7;
const int _MINWORKINGDAYSMOVE = 8;
const int _CURRICULUMCOMPACTNESSMOVE = 9;
const int _LECTURE3  = 10;
const int _LECTURE4 = 11;
const int _KEMPEEXT = 12;
const int _KEMPERESTSALA  = 13;
const int _KEMPESALAVAZIA = 14;
const int _KEMPEEXTRESTSALA = 15;
const int _ROOMMOVE2 = 16;

const int _MOVE_SWAP = 0;
const int _MOVE_SWAP_KEMPE_30 = 20;
const int _MOVE_SWAP_KEMPE_20 = 21;
const int _MOVE_SWAP_KEMPE_10 = 22;
const int _LM3_KEMPE_10 = 23;
const int _LM3_KEMPE_20 = 24;
const int _LM3_KEMPE_30 = 25;
const int _TIME_ROOM = 30;

SimulatedAnnealing::SimulatedAnnealing(Problema* p, int pinMaxIterSemMelhora, double tempInicial, double tempFinal, double taxaDecaimentoTemp) {
	nMaxIterSemMelhora = pinMaxIterSemMelhora;
	problema = p;
	temperaturaInicial = tempInicial;
	temperaturaFinal   = tempFinal;
	taxaDecaimentoTemperatura = taxaDecaimentoTemp;
	aceitaPioraSA = 1;
}

SimulatedAnnealing::~SimulatedAnnealing() { }


Movimento* SimulatedAnnealing::obtemMovimento(Individuo* ind, int movimento){
	Movimento* mov;
	float sorteio;
	int movARealizar;

	switch( movimento ){
		case _MOVE_SWAP:
			sorteio = (float) rand() / RAND_MAX;
			if( sorteio < 0.5 ) movARealizar = _MOVE;
			else  movARealizar = _SWAP;
			break;
		case _MOVE_SWAP_KEMPE_30:
			sorteio = (float) rand() / RAND_MAX;
			if( sorteio < 0.35 ) movARealizar = _MOVE;
			else if( sorteio < 0.70 ) movARealizar = _SWAP;
			else movARealizar = _KEMPE;
			break;
		case _MOVE_SWAP_KEMPE_20:
			sorteio = (float) rand() / RAND_MAX;
			if( sorteio < 0.4 ) movARealizar = _MOVE;
			else if( sorteio < 0.8 ) movARealizar = _SWAP;
			else movARealizar = _KEMPE;
			break;
		case _MOVE_SWAP_KEMPE_10:
			sorteio = (float) rand() / RAND_MAX;
			if( sorteio < 0.45 ) movARealizar = _MOVE;
			else if( sorteio < 0.90 ) movARealizar = _SWAP;
			else movARealizar = _KEMPE;
			break;
		case _TIME_ROOM:
			sorteio = (float) rand() / RAND_MAX;
			if( sorteio < 0.5 ) movARealizar = _TIMEMOVE;
			else  movARealizar = _ROOMMOVE;
			break;
		case _LM3_KEMPE_10:
			sorteio = (float) rand() / RAND_MAX;
			if( sorteio < 0.9 ) movARealizar = _LECTURE3;
			else  movARealizar = _KEMPE;
			break;
		case _LM3_KEMPE_20:
			sorteio = (float) rand() / RAND_MAX;
			if( sorteio < 0.8 ) movARealizar = _LECTURE3;
			else  movARealizar = _KEMPE;
			break;
		case _LM3_KEMPE_30:
			sorteio = (float) rand() / RAND_MAX;
			if( sorteio < 0.7 ) movARealizar = _LECTURE3;
			else  movARealizar = _KEMPE;
			break;
		default: movARealizar=movimento;
	}


	switch(movARealizar){
	case _MOVE:  mov = new Move(problema, ind);
			     nMoves++;
			     if( mov->deltaFit < 0 ){
			    	 nMovesMelhora++;
			     }
			     else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
			 	 	nMovesValidos++;
			 	 }
			     else nMovesInvalido++;
		         break;
	case _SWAP:  mov = new Swap(problema, ind);
			     nSwaps++;
			     if( mov->deltaFit < 0 ){
			    	 nSwapsMelhora++;
			     }
			     else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
			 	 	nSwapsValidos++;
			 	 }
			     else nSwapsInvalido++;
		         break;
	case _KEMPE: mov = new CadeiadeKempe(problema, ind);
				 nKempes++;
			     if( mov->deltaFit < 0 ){
			    	 nKempesMelhora++;
			     }
			     else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
			 	 	nKempesValidos++;
			 	 }
			     else nKempesInvalido++;
		         break;
	case _KEMPEEXT: mov = new CadeiadeKempeExtendido(problema, ind);
				 nKempes++;
			     if( mov->deltaFit < 0 ){
			    	 nKempesMelhora++;
			     }
			     else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
			 	 	nKempesValidos++;
			 	 }
			     else nKempesInvalido++;
		         break;
	case _KEMPEEXTRESTSALA: mov = new CadeiadeKempeEstendidaRestrSala(problema, ind);
				 nKempes++;
			     if( mov->deltaFit < 0 ){
			    	 nKempesMelhora++;
			     }
			     else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
			 	 	nKempesValidos++;
			 	 }
			     else nKempesInvalido++;
		         break;
	case _LECTURE3: mov = new LectureMove3(problema, ind);
					nLectureMoves++;
					if( mov->deltaFit < 0 ){
						nLectureMovesMelhora++;
					}
					else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
						nLectureMovesValidos++;
					}
					else nLectureMovesInvalido++;
					break;
	case _TIMEMOVE: mov = new TimeMove2(problema, ind);
				 nTimeMoves++;
			     if( mov->deltaFit < 0 ){
			    	 nTimeMovesMelhora++;
			     }
			     else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
			 	 	nTimeMovesValidos++;
			 	 }
			     else nTimeMovesInvalido++;
		         break;
	case _ROOMMOVE: mov = new RoomMove2(problema, ind);
					nRoomMoves++;
					if( mov->deltaFit < 0 ){
						nRoomMovesMelhora++;
					}
					else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
						nRoomMovesValidos++;
					}
					else nRoomMovesInvalido++;
					break;
	case _LECTUREMOVE: mov = new LectureMove(problema, ind);
					nLectureMoves++;
					if( mov->deltaFit < 0 ){
						nLectureMovesMelhora++;
					}
					else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
						nLectureMovesValidos++;
					}
					else nLectureMovesInvalido++;
					break;
  case _LECTURE4: mov = new LectureMove4(problema, ind);
					nLectureMoves++;
					if( mov->deltaFit < 0 ){
						nLectureMovesMelhora++;
					}
					else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
						nLectureMovesValidos++;
					}
					else nLectureMovesInvalido++;
					break;
  case _ROOMSTABILITYMOVE: mov = new RoomStabilityMove(problema, ind);
          nRoomStabilityMoves++;
          if( mov->deltaFit < 0 ){
            nRoomStabilityMovesMelhora++;
          }
          else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
            nRoomStabilityMovesValidos++;
          }
          else nRoomStabilityMovesInvalido++;
          break;
  case _MINWORKINGDAYSMOVE: mov = new MinWorkingDaysMove(problema, ind);
          nMinWorkingDaysMoves++;
          if( mov->deltaFit < 0 ){
            nMinWorkingDaysMovesMelhora++;
          }
          else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
            nMinWorkingDaysMovesValidos++;
          }
          else nMinWorkingDaysMovesInvalido++;
          break;
  case _CURRICULUMCOMPACTNESSMOVE: mov = new CurriculumCompactnessMove(problema, ind);
          nCurriculumCompactnessMoves++;
          if( mov->deltaFit < 0 ){
            nCurriculumCompactnessMovesMelhora++;
          }
          else if( (mov->deltaHard <= 0) || (mov->deltaFit < 10000) ){
            nCurriculumCompactnessMovesValidos++;
          }
          else nCurriculumCompactnessMovesInvalido++;
          break;
		default: fprintf(stderr, "Movimento %d invalido!\n\n", movARealizar);
				 exit(0);
	}
	//nMovimentosRealizados++;
	//printf(";%4d; %3d; %3d; %3d\n", mov->deltaFit, (mov->a1->sala->numeroSequencial*problema->nHorarios)+mov->a1->horario->horario, (mov->a2->sala->numeroSequencial*problema->nHorarios)+mov->a2->horario->horario);

	return mov;
}

Individuo* SimulatedAnnealing::executa(Individuo* bestInd, int movimento, int imprimeDetalhes){
	Movimento *vizinho;
	Individuo* melhorInd, *solucaoAtual;
	double prob;

	long N, i;
	double tAtual = temperaturaInicial;

	solucaoAtual = bestInd;
	melhorInd = new Individuo(solucaoAtual);

	nIteracoesExecutadas = 0;
	do {
		N = 500;
		i = 0;
		do {
			vizinho = obtemMovimento(solucaoAtual, movimento);
			if( vizinho->deltaFit <= 0 ){// função objetivo decresceu
				vizinho->aplicaMovimento();
				nMovimentosRealizados++;

				//printf("atualiza melhor individuo\n");
				if (solucaoAtual->fitness < melhorInd->fitness) {
					delete(melhorInd);
					melhorInd = new Individuo(solucaoAtual);
				}
			} else if( vizinho->deltaFit < 3000 ){
				i++;
				// calcula probabilidade de aceitação
				prob = pow(M_E, -vizinho->deltaFit / tAtual);
				if( aceitaPioraSA && (((float) rand() / RAND_MAX) <= prob) ){
					vizinho->aplicaMovimento();
					nMovimentosRealizados++;
				}
			}
			delete(vizinho);
			N--;
			nIteracoesExecutadas++;

			if( i >= nMaxIterSemMelhora ) break;
		} while( N > 0 );

		if( i >= nMaxIterSemMelhora ) break;
		tAtual *= taxaDecaimentoTemperatura;
	} while (tAtual > temperaturaFinal);

	delete(solucaoAtual);
	return melhorInd;
}

void SimulatedAnnealing::imprimeExecucao(){
	printf("Maximo de Iteracoes sem Melhora: %d\n", nMaxIterSemMelhora);
	printf("Variacao de temperatura:  %lf a %lf, a uma taxa de %lf\n", temperaturaInicial, temperaturaFinal, taxaDecaimentoTemperatura);
	printf("\nNumero de Movimentos Realizados: %d\n", nMovimentosRealizados);
	printf("Total de Movimentos   :  nMoves: %7d nSwaps: %7d nKempes: %7d nTimeMoves: %7d nRoomMoves: %7d nLectureMoves: %7d nRoomStabilityMoves: %7d nMinWorkingDaysMoves: %7d nCurriculumCompactnessMoves: %7d\n", nMoves, nSwaps, nKempes, nTimeMoves, nRoomMoves, nLectureMoves, nRoomStabilityMoves, nMinWorkingDaysMoves, nCurriculumCompactnessMoves);
	printf("Movimentos Invalidos  :  nMoves: %7d nSwaps: %7d nKempes: %7d nTimeMoves: %7d nRoomMoves: %7d nLectureMoves: %7d nRoomStabilityMoves: %7d nMinWorkingDaysMoves: %7d nCurriculumCompactnessMoves: %7d\n", nMovesInvalido, nSwapsInvalido, nKempesInvalido, nTimeMovesInvalido, nRoomMovesInvalido, nLectureMovesInvalido, nRoomStabilityMovesInvalido, nMinWorkingDaysMovesInvalido, nCurriculumCompactnessMovesInvalido);
	printf("Movimentos Validos    :  nMoves: %7d nSwaps: %7d nKempes: %7d nTimeMoves: %7d nRoomMoves: %7d nLectureMoves: %7d nRoomStabilityMoves: %7d nMinWorkingDaysMoves: %7d nCurriculumCompactnessMoves: %7d\n", nMovesValidos, nSwapsValidos, nKempesValidos, nTimeMovesValidos, nRoomMovesValidos, nLectureMovesValidos, nRoomStabilityMovesValidos, nMinWorkingDaysMovesValidos, nCurriculumCompactnessMovesValidos);
	printf("Movimentos com Melhora:  nMoves: %7d nSwaps: %7d nKempes: %7d nTimeMoves: %7d nRoomMoves: %7d nLectureMoves: %7d nRoomStabilityMoves: %7d nMinWorkingDaysMoves: %7d nCurriculumCompactnessMoves: %7d\n", nMovesMelhora, nSwapsMelhora, nKempesMelhora, nTimeMovesMelhora, nRoomMovesMelhora, nLectureMovesMelhora, nRoomStabilityMovesMelhora, nMinWorkingDaysMovesMelhora, nCurriculumCompactnessMovesMelhora);
}
