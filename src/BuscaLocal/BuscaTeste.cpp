/*
 * HillClimbing.cpp
 *
 *  Created on: May 26, 2015
 *      Author: erika
 */

#include <stdlib.h>
#include <vector>

#include "BuscaTeste.h"
#include "../Vizinhancas/Move.h"
#include "../Vizinhancas/Swap.h"
#include "../Vizinhancas/TimeMove.h"
#include "../Vizinhancas/TimeMove2.h"
#include "../Vizinhancas/CadeiadeKempe.h"
#include "../Vizinhancas/CadeiadeKempeRestricaoSala.h"
#include "../Vizinhancas/CadeiaKempeCompletaSalaVazia.h"
#include "../Vizinhancas/CadeiadeKempeEstendidaRestrSala.h"
#include "../Vizinhancas/CadeiadeKempeExtendido.h"
#include "../Vizinhancas/RoomMove.h"
#include "../Vizinhancas/RoomMove2.h"
#include "../Vizinhancas/LectureMove.h"
#include "../Vizinhancas/LectureMove2.h"
#include "../Vizinhancas/LectureMove3.h"
#include "../Vizinhancas/LectureMove4.h"
#include "../Vizinhancas/RoomStabilityMove.h"
#include "../Vizinhancas/MinWorkingDaysMove.h"
#include "../Vizinhancas/CurriculumCompactnessMove.h"

const int _MOVE = 1;
const int _SWAP = 2;
const int _KEMPE = 3;
const int _TIMEMOVE = 4;
const int _ROOMMOVE = 5;
const int _LECTUREMOVE = 6;
const int _ROOMSTABILITYMOVE = 7;
const int _MINWORKINGDAYSMOVE = 8;
const int _CURRICULUMCOMPACTNESSMOVE = 9;
const int _LECTURE3 = 10;
const int _LECTURE4 = 11;
const int _KEMPEEXT = 12;
const int _KEMPERESTSALA  = 13;
const int _KEMPESALAVAZIA = 14;
const int _KEMPEEXTRESTSALA = 15;
const int _ROOMMOVE2 = 16;
const int _LECTURE2 = 17;

const int _MOVE_SWAP = 0;
const int _MOVE_SWAP_KEMPE_30 = 20;
const int _MOVE_SWAP_KEMPE_20 = 21;
const int _MOVE_SWAP_KEMPE_10 = 22;
const int _TIME_ROOM = 30;

#define N 50

BuscaTeste::BuscaTeste(Problema* p) {
	problema = p;
}

BuscaTeste::~BuscaTeste() { }

Individuo* BuscaTeste::executaSDKempe(Individuo* ind) {
	Movimento* mov;
	int h1, h2;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;
	int cont = 0;

	//Cria movimento
	while (1) {
		nIteracoesExecutadas++;

		for (h1 = 0; h1 < (int)ind->aulasAlocadas.size(); h1++) {
			for (h2 = 0; h2 < (int)ind->aulasAlocadas.size(); h2++) {
				if (ind->aulasAlocadas[h1]->aula->disciplina->numeroSequencial != ind->aulasAlocadas[h2]->aula->disciplina->numeroSequencial) {
					mov = new CadeiadeKempe(ind->p, ind, h1, h2);
//					if( mov->deltaFit < 0 ) 
//						printf("new CadeiadeKempe %3d %3d => %5d\n", h1, h2, mov->deltaFit);

					nKempes++;
					if (mov->deltaFit < 0)         nKempesMelhora++;
					else if (mov->deltaFit < 2000) nKempesValidos++;
					else                           nKempesInvalido++;
					
					if (mov->deltaFit < 0)         cont++;

					if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
						while ( !melhorMov.empty()){
							delete(melhorMov.back());
							melhorMov.pop_back();
						}
						melhorMov.push_back(mov);
					} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
						melhorMov.push_back(mov);
					} else
						delete (mov);
						
					if ( cont >= 5 ) break;
				}
			}
			if ( cont >= 5 ) break;
		}

		if (melhorMov[0]->deltaFit < 0) {
			//printf("%d\n", (melhorMov[0]->deltaFit * (-1)));
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		continua = 0;
		if (!continua) break;
	};
	//printf("*********************************************\n");
	return ind;
}

Individuo* BuscaTeste::executaSDKempeEstendido(Individuo* ind) {
	Movimento* mov;
	int h1, h2;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;
	int cont = 0;

	//Cria movimento
	while (1) {
		nIteracoesExecutadas++;
		//printf("\nIteracao %d\n", i++);

		for (h1 = 0; h1 != ind->p->nHorarios; h1++) {
			for (h2 = 0; h2 != ind->p->nHorarios; h2++) {

				if (h1 != h2) {
					mov = new CadeiadeKempeExtendido(ind->p, ind, h1, h2);
					
					nKempes++;
					if (mov->deltaFit < 0)         nKempesMelhora++;
					else if (mov->deltaFit < 2000) nKempesValidos++;
					else                           nKempesInvalido++;

					if (mov->deltaFit < 0)         cont++;
					
					if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
						while ( !melhorMov.empty()){
							delete(melhorMov.back());
							melhorMov.pop_back();
						}
						melhorMov.push_back(mov);
					} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
						melhorMov.push_back(mov);
					} else
						delete (mov);
				}
				if ( cont >= 25 ) break;
			}
			if ( cont >= 25 ) break;
		}

		if (melhorMov[0]->deltaFit < 0) {
			//printf("%d\n", (melhorMov[0]->deltaFit * (-1)));
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		continua = 0;
		if (!continua) break;

	};
	return ind;
}


Individuo* BuscaTeste::executa(Individuo* bestInd, const int movimento) {
	switch (movimento) {

	case _KEMPE:
		bestInd = executaSDKempe(bestInd);
		break;
	case _KEMPEEXT:
		bestInd = executaSDKempeEstendido(bestInd);
		break;
	default:
		fprintf(stderr, "Movimento %d invalido!\n\n", movimento);
		fprintf(stderr, "Movimentos validos para teste:\n");
		//fprintf(stderr, "\tMOVE  (%d)\n\tSWAP  (%d)\n", _MOVE, _SWAP);
		fprintf(stderr, "\tKempe Simples(%d)\n\tKempe estendido (%d)\n", _KEMPE, _KEMPEEXT);
//		fprintf(stderr, "\tKempe adiciona sala vazia (%d)\n\tKempe com restricao de sala (%d)\n", _KEMPESALAVAZIA, _KEMPERESTSALA);
//		fprintf(stderr, "\tKempe estendido com restrição de sala (%d)\n", _KEMPEEXTRESTSALA);
//		fprintf(stderr, "\tTimeMove   (%d)\n", _TIMEMOVE);
//		fprintf(stderr, "\tRoomMove 1-2 (%d, %d)\n", _ROOMMOVE, _ROOMMOVE2);
//		fprintf(stderr, "\tLectureMove 1-4 (%d, %d, %d, %d)\n", _LECTUREMOVE, _LECTURE2, _LECTURE3, _LECTURE4);
//		fprintf(stderr, "\tRoomStabilityMove  (%d)\n\t \n", _ROOMSTABILITYMOVE);
//		fprintf(stderr, "\tMinWorkingDaysMove (%d)\n\t \n", _MINWORKINGDAYSMOVE);
		exit(0);
	}

	return bestInd;
}

void BuscaTeste::imprimeExecucao() {
	printf("\nNumero de Movimentos Realizados: %d\n", nMovimentosRealizados);
	printf(
			"Total de Movimentos   :  nMoves: %7d nSwaps: %7d nKempes: %7d nTimeMoves: %7d nRoomMoves: %7d nLectureMoves: %7d nRoomStabilityMoves: %7d nMinWorkingDaysMoves: %7d nCurriculumCompactnessMoves: %7d\n",
			nMoves, nSwaps, nKempes, nTimeMoves, nRoomMoves, nLectureMoves,
			nRoomStabilityMoves, nMinWorkingDaysMoves,
			nCurriculumCompactnessMoves);
	printf(
			"Movimentos Invalidos  :  nMoves: %7d nSwaps: %7d nKempes: %7d nTimeMoves: %7d nRoomMoves: %7d nLectureMoves: %7d nRoomStabilityMoves: %7d nMinWorkingDaysMoves: %7d nCurriculumCompactnessMoves: %7d\n",
			nMovesInvalido, nSwapsInvalido, nKempesInvalido, nTimeMovesInvalido,
			nRoomMovesInvalido, nLectureMovesInvalido,
			nRoomStabilityMovesInvalido, nMinWorkingDaysMovesInvalido,
			nCurriculumCompactnessMovesInvalido);
	printf(
			"Movimentos Validos    :  nMoves: %7d nSwaps: %7d nKempes: %7d nTimeMoves: %7d nRoomMoves: %7d nLectureMoves: %7d nRoomStabilityMoves: %7d nMinWorkingDaysMoves: %7d nCurriculumCompactnessMoves: %7d\n",
			nMovesValidos, nSwapsValidos, nKempesValidos, nTimeMovesValidos,
			nRoomMovesValidos, nLectureMovesValidos, nRoomStabilityMovesValidos,
			nMinWorkingDaysMovesValidos, nCurriculumCompactnessMovesValidos);
	printf(
			"Movimentos com Melhora:  nMoves: %7d nSwaps: %7d nKempes: %7d nTimeMoves: %7d nRoomMoves: %7d nLectureMoves: %7d nRoomStabilityMoves: %7d nMinWorkingDaysMoves: %7d nCurriculumCompactnessMoves: %7d\n",
			nMovesMelhora, nSwapsMelhora, nKempesMelhora, nTimeMovesMelhora,
			nRoomMovesMelhora, nLectureMovesMelhora, nRoomStabilityMovesMelhora,
			nMinWorkingDaysMovesMelhora, nCurriculumCompactnessMovesMelhora);
}

