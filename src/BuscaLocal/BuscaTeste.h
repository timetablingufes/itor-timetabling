/*
 * BuscaTeste.h
 *
 *  Created on: May 3, 2016
 *      Author: erika
 */

#ifndef BuscaTeste_H_
#define BuscaTeste_H_

#include "BuscaLocal.h"

class Problema;
class Individuo;
class Movimento;

class BuscaTeste : public BuscaLocal {
public:
	Problema* problema;

	BuscaTeste(Problema* p);
	virtual ~BuscaTeste();

	Individuo* executa(Individuo* bestInd, const int movimento);
	void imprimeExecucao();

private:
	Individuo* executaSDKempe(Individuo* ind);
	Individuo* executaSDKempeEstendido(Individuo* ind);
};

#endif /* BuscaTeste_H_ */
