/*
 * SteepestDescent.h
 *
 *  Created on: May 3, 2016
 *      Author: erika
 */

#ifndef STEEPESTDESCENT_H_
#define STEEPESTDESCENT_H_

#include "BuscaLocal.h"

class Problema;
class Individuo;

class SteepestDescent : public BuscaLocal {
public:
	Problema* problema;

	SteepestDescent(Problema* p);
	virtual ~SteepestDescent();

	Individuo* executa(Individuo* bestInd, const int movimento);
	void imprimeExecucao();

protected:
	Individuo* executaSDMoveSwap(Individuo* ind);
	Individuo* executaSDMove(Individuo* ind);
	Individuo* executaSDSwap(Individuo* ind);
	Individuo* executaSDKempe(Individuo* ind);
	Individuo* executaSDKempeEstendido(Individuo* ind);
	Individuo* executaSDKempeRestricaoSala(Individuo* ind);
	Individuo* executaSDKempeSalaVazia(Individuo* ind);
	Individuo* executaSDKempeEstendidoRestricaoSala(Individuo* ind);
	Individuo* executaSDTimeMove(Individuo* ind);
	Individuo* executaSDRoomMove(Individuo* ind);
	Individuo* executaSDRoomMove2(Individuo* ind);
	Individuo* executaSDLectureMove1(Individuo* ind);
	Individuo* executaSDLectureMove2(Individuo* ind);
	Individuo* executaSDLectureMove3(Individuo* ind);
	Individuo* executaSDLectureMove4(Individuo* ind);
	Individuo* executaSDLectureMove3e4(Individuo* ind);
	Individuo* executaSDMinWorkingDays(Individuo* ind);
	Individuo* executaSDRoomStabilityMove(Individuo* ind);

	Individuo* executaSDKempeEstendidoRestricaoSala(Individuo* ind, int nIterMaxSD);
	Individuo* executaSDMoveSwap(Individuo* ind, int nIterMax);
	Individuo* executaSDMinWorkingDays(Individuo* ind, int nIterMax);
	Individuo* executaSDRoomStabilityMove(Individuo* ind, int nIterMax);
	
	Individuo* executaSDTimeMove(Individuo* ind, int nIterMax);
	Individuo* executaSDLectureMove3(Individuo* ind, int nIterMax);
	Individuo* executaSDLectureMove3e4(Individuo* ind, int nIterMax);
	Individuo* executaSDRoomMove(Individuo* ind, int nIterMax);
	
	Individuo* executaSDLMRM(Individuo* ind, int nIterMax);
	Individuo* executaSDLMTM(Individuo* ind, int nIterMax);
	Individuo* executaSDTMRM(Individuo* ind, int nIterMax);
	Individuo* executaSDLMRMTM(Individuo* ind, int nIterMax);

};

#endif /* STEEPESTDESCENT_H_ */
