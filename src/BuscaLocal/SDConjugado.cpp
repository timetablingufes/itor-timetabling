/*
 * SDConjugado.cpp
 *
 *  Created on: 12/12/2016
 *      Author: labotimlocal
 */

#include "SDConjugado.h"

#include <stdlib.h>
#include <vector>

#include "../Vizinhancas/Move.h"
#include "../Vizinhancas/Swap.h"
#include "../Vizinhancas/TimeMove.h"
#include "../Vizinhancas/TimeMove2.h"
#include "../Vizinhancas/CadeiadeKempe.h"
#include "../Vizinhancas/CadeiadeKempeRestricaoSala.h"
#include "../Vizinhancas/CadeiaKempeCompletaSalaVazia.h"
#include "../Vizinhancas/CadeiadeKempeEstendidaRestrSala.h"
#include "../Vizinhancas/CadeiadeKempeExtendido.h"
#include "../Vizinhancas/RoomMove.h"
#include "../Vizinhancas/RoomMove2.h"
#include "../Vizinhancas/LectureMove.h"
#include "../Vizinhancas/LectureMove2.h"
#include "../Vizinhancas/LectureMove3.h"
#include "../Vizinhancas/LectureMove4.h"
#include "../Vizinhancas/RoomStabilityMove.h"
#include "../Vizinhancas/MinWorkingDaysMove.h"
#include "../Vizinhancas/CurriculumCompactnessMove.h"

const int _MOVE = 1;
const int _SWAP = 2;
const int _KEMPE = 3;
const int _TIMEMOVE = 4;
const int _ROOMMOVE = 5;
const int _LECTUREMOVE = 6;
const int _ROOMSTABILITYMOVE = 7;
const int _MINWORKINGDAYSMOVE = 8;
const int _CURRICULUMCOMPACTNESSMOVE = 9;
const int _LECTURE3 = 10;
const int _LECTURE4 = 11;
const int _KEMPEEXT = 12;
const int _KEMPERESTSALA  = 13;
const int _KEMPESALAVAZIA = 14;
const int _KEMPEEXTRESTSALA = 15;
const int _ROOMMOVE2 = 16;
const int _LECTURE2 = 17;

const int _MOVE_SWAP = 0;
const int _MOVE_SWAP_KEMPE_30 = 20;
const int _MOVE_SWAP_KEMPE_20 = 21;
const int _MOVE_SWAP_KEMPE_10 = 22;
const int _TIME_ROOM = 30;

const int _MOVE_SWAP_KERS = 50;
const int _MOVE_SWAP_MWD  = 51;
const int _MOVE_SWAP_RSM  = 52;
const int _LM3_KERS       = 53;
const int _LM3_MWD        = 54;
const int _LM3_RSM        = 55;
const int _LM3_LM4_KERS   = 56;
const int _LM3_LM4_MWD    = 57;
const int _LM3_LM4_RSM    = 58;
const int _LM3_RM_TM      = 59;
const int _LM3_RM         = 60;
const int _LM3_TM         = 61;
const int _RM_TM          = 62;


#define nIterMaxSD 2000
#define nIterMaxPrincipal 20
#define nIterMaxPertubacao 5
#define nIterMaxBalanceado 10

SDConjugado::SDConjugado(Problema* p) :
	SteepestDescent(p){

}

Individuo* SDConjugado::executaSDMoveSwapKERS(Individuo* ind){
	int foAnterior;
	int nIterMax = nIterMaxSD;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {

		foAnterior = ind->fitness;
		ind = executaSDMoveSwap(ind, nIterMaxPrincipal);
		ind = executaSDKempeEstendidoRestricaoSala(ind, nIterMaxPertubacao);

		if( foAnterior == ind->fitness ) break;
	};

	//printf("*********************************************\n");
	return ind;
}

Individuo* SDConjugado::executaSDMoveSwapMWD(Individuo* ind){
	int foAnterior;
	int nIterMax = nIterMaxSD;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {

		foAnterior = ind->fitness;
		ind = executaSDMoveSwap(ind, nIterMaxPrincipal);
		ind = executaSDMinWorkingDays(ind, nIterMaxPertubacao);

		if( foAnterior == ind->fitness ) break;
	};

	return ind;
}

Individuo* SDConjugado::executaSDMoveSwapRSM(Individuo* ind){
	int foAnterior;
	int nIterMax = nIterMaxSD;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {

		foAnterior = ind->fitness;
		ind = executaSDMoveSwap(ind);
		ind = executaSDRoomStabilityMove(ind, nIterMaxPertubacao);

		if( foAnterior == ind->fitness ) break;
	};

	//printf("*********************************************\n");
	return ind;
}


Individuo* SDConjugado::executaSDLectureMove3KERS(Individuo* ind){
	int foAnterior;
	int nIterMax = nIterMaxSD;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {

		foAnterior = ind->fitness;
		ind = executaSDLectureMove3(ind, nIterMaxPrincipal);
		ind = executaSDKempeEstendidoRestricaoSala(ind, nIterMaxPertubacao);

		if( foAnterior == ind->fitness ) break;
	};

	//printf("*********************************************\n");
	return ind;
}

Individuo* SDConjugado::executaSDLectureMove3MWD(Individuo* ind){
	int foAnterior;
	int nIterMax = nIterMaxSD;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {

		foAnterior = ind->fitness;
		ind = executaSDLectureMove3(ind, nIterMaxPrincipal);
		ind = executaSDMinWorkingDays(ind, nIterMaxPertubacao);

		if( foAnterior == ind->fitness ) break;
	};

	return ind;
}

Individuo* SDConjugado::executaSDLectureMove3RSM(Individuo* ind){
	int foAnterior;
	int nIterMax = nIterMaxSD;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {

		foAnterior = ind->fitness;
		ind = executaSDLectureMove3(ind, nIterMaxPrincipal);
		ind = executaSDRoomStabilityMove(ind, nIterMaxPertubacao);

		if( foAnterior == ind->fitness ) break;
	};

	return ind;
}


Individuo* SDConjugado::executaSDLectureMove3e4KERS(Individuo* ind){
	int foAnterior;
	int nIterMax = nIterMaxSD;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {

		foAnterior = ind->fitness;
		ind = executaSDLectureMove3e4(ind, nIterMaxPrincipal);
		ind = executaSDKempeEstendidoRestricaoSala(ind, nIterMaxPertubacao);

		if( foAnterior == ind->fitness ) break;
	};

	return ind;
}

Individuo* SDConjugado::executaSDLectureMove3e4MWD(Individuo* ind){
	int foAnterior;
	int nIterMax = nIterMaxSD;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {

		foAnterior = ind->fitness;
		ind = executaSDLectureMove3e4(ind, nIterMaxPrincipal);
		ind = executaSDMinWorkingDays(ind, nIterMaxPertubacao);

		if( foAnterior == ind->fitness ) break;
	};

	return ind;
}

Individuo* SDConjugado::executaSDLectureMove3e4RSM(Individuo* ind){
	int foAnterior;
	int nIterMax = nIterMaxSD;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {

		foAnterior = ind->fitness;
		ind = executaSDLectureMove3e4(ind, nIterMaxPrincipal);
		ind = executaSDRoomStabilityMove(ind, nIterMaxPertubacao);

		if( foAnterior == ind->fitness ) break;
	};

	return ind;
}



Individuo* SDConjugado::executaSDLectureMove3eRoomMoveeTimeMove(Individuo* ind){
	int foAnterior;
	int nIterMax = nIterMaxSD;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {

		foAnterior = ind->fitness;
		ind = executaSDLectureMove3(ind, nIterMaxBalanceado);
		ind = executaSDRoomMove(ind, nIterMaxBalanceado);
		ind = executaSDTimeMove(ind, nIterMaxBalanceado);

		if( foAnterior == ind->fitness ) break;
	};

	return ind;
}
Individuo* SDConjugado::executaSDLectureMove3eRoomMove(Individuo* ind){
	int foAnterior;
	int nIterMax = nIterMaxSD;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {

		foAnterior = ind->fitness;
		ind = executaSDLectureMove3(ind, nIterMaxBalanceado);
		ind = executaSDRoomMove(ind, nIterMaxBalanceado);

		if( foAnterior == ind->fitness ) break;
	};

	return ind;
}
Individuo* SDConjugado::executaSDLectureMove3eTimeMove(Individuo* ind){
	int foAnterior;
	int nIterMax = nIterMaxSD;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {

		foAnterior = ind->fitness;
		ind = executaSDLectureMove3(ind, nIterMaxBalanceado);
		ind = executaSDTimeMove(ind, nIterMaxBalanceado);

		if( foAnterior == ind->fitness ) break;
	};

	return ind;
}
Individuo* SDConjugado::executaSDRoomMoveeTimeMove(Individuo* ind){
	int foAnterior;
	int nIterMax = nIterMaxSD;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {

		foAnterior = ind->fitness;
		ind = executaSDRoomMove(ind, nIterMaxBalanceado);
		ind = executaSDTimeMove(ind, nIterMaxBalanceado);

		if( foAnterior == ind->fitness ) break;
	};

	return ind;
}


Individuo* SDConjugado::executa(Individuo* bestInd, const int movimento) {
	switch (movimento) {
	case _MOVE_SWAP:
		bestInd = executaSDMoveSwap(bestInd);
		break;
	case _MOVE:
		bestInd = executaSDMove(bestInd);
		break;
	case _SWAP:
		bestInd = executaSDSwap(bestInd);
		break;
	case _KEMPE:
		bestInd = executaSDKempe(bestInd);
		break;
	case _KEMPEEXT:
		bestInd = executaSDKempeEstendido(bestInd);
		break;
	case _KEMPERESTSALA:
		bestInd = executaSDKempeRestricaoSala(bestInd);
		break;
	case _KEMPESALAVAZIA:
		bestInd = executaSDKempeSalaVazia(bestInd);
		break;
	case _KEMPEEXTRESTSALA:
		bestInd = executaSDKempeEstendidoRestricaoSala(bestInd);
		break;
	case _TIMEMOVE:
		bestInd = executaSDTimeMove(bestInd);
		break;
	case _ROOMMOVE:
		bestInd = executaSDRoomMove(bestInd);
		break;
	case _ROOMMOVE2:
		bestInd = executaSDRoomMove2(bestInd);
		break;
	case _LECTUREMOVE:
		bestInd = executaSDLectureMove1(bestInd);
		break;
	case _LECTURE2:
		bestInd = executaSDLectureMove2(bestInd);
		break;
	case _LECTURE3:
		bestInd = executaSDLectureMove3(bestInd);
		break;
	case _LECTURE4:
		bestInd = executaSDLectureMove4(bestInd);
		break;
	case _ROOMSTABILITYMOVE:
		bestInd = executaSDRoomStabilityMove(bestInd);
		break;
	case _MINWORKINGDAYSMOVE:
		bestInd = executaSDMinWorkingDays(bestInd);
		break;
	case _MOVE_SWAP_KERS:
		bestInd = executaSDMoveSwapKERS(bestInd);
		break;
	case _MOVE_SWAP_MWD:
		bestInd = executaSDMoveSwapMWD(bestInd);
		break;
	case _MOVE_SWAP_RSM:
		bestInd = executaSDMoveSwapRSM(bestInd);
		break;
	case _LM3_KERS:
		bestInd = executaSDLectureMove3KERS(bestInd);
		break;
	case _LM3_MWD:
		bestInd = executaSDLectureMove3MWD(bestInd);
		break;
	case _LM3_RSM:
		bestInd = executaSDLectureMove3RSM(bestInd);
		break;
	case _LM3_LM4_KERS:
		bestInd = executaSDLectureMove3e4KERS(bestInd);
		break;
	case _LM3_LM4_MWD:
		bestInd = executaSDLectureMove3e4MWD(bestInd);
		break;
	case _LM3_LM4_RSM:
		bestInd = executaSDLectureMove3e4RSM(bestInd);
		break;
	case _LM3_RM_TM:
		bestInd = executaSDLMRMTM(bestInd, 99999);
		break;
	case _LM3_RM:
		bestInd = executaSDLMRM(bestInd, 99999);
		break;
	case _LM3_TM:
		bestInd = executaSDLMTM(bestInd, 99999);
		break;
	case _RM_TM:
		bestInd = executaSDTMRM(bestInd, 99999);
		break;
	default:
		fprintf(stderr, "Movimento %d invalido!\n\n", movimento);
		fprintf(stderr, "Movimentos validos para o SD Conjugado:\n");
		fprintf(stderr, "\tMOVE  (%d)\n\tSWAP  (%d)\n", _MOVE, _SWAP);
		fprintf(stderr, "\tKempe Simples(%d)\n\tKempe estendido (%d)\n", _KEMPE, _KEMPEEXT);
		fprintf(stderr, "\tKempe adiciona sala vazia (%d)\n\tKempe com restricao de sala (%d)\n", _KEMPESALAVAZIA, _KEMPERESTSALA);
		fprintf(stderr, "\tKempe estendido com restrição de sala (%d)\n", _KEMPEEXTRESTSALA);
		fprintf(stderr, "\tTimeMove   (%d)\n", _TIMEMOVE);
		fprintf(stderr, "\tRoomMove 1-2 (%d, %d)\n", _ROOMMOVE, _ROOMMOVE2);
		fprintf(stderr, "\tLectureMove 1-4 (%d, %d, %d, %d)\n", _LECTUREMOVE, _LECTURE2, _LECTURE3, _LECTURE4);
		fprintf(stderr, "\tRoomStabilityMove  (%d)\n\t \n", _ROOMSTABILITYMOVE);
		fprintf(stderr, "\tMinWorkingDaysMove (%d)\n\t \n", _MINWORKINGDAYSMOVE);
		fprintf(stderr, "\n");
		fprintf(stderr, "\t(Move + Swap) / Kempe estendido com restrição de sala (%d)\n", _MOVE_SWAP_KERS);
		fprintf(stderr, "\t(Move + Swap) / Minimum Working Days Move  (%d)\n", _MOVE_SWAP_MWD);
		fprintf(stderr, "\t(Move + Swap) / Room Stability Move  (%d)\n", _MOVE_SWAP_RSM);
		fprintf(stderr, "\t(Lecture Move 3) / Kempe estendido com restrição de sala (%d)\n", _LM3_KERS);
		fprintf(stderr, "\t(Lecture Move 3) / Minimum Working Days Move  (%d)\n", _LM3_MWD);
		fprintf(stderr, "\t(Lecture Move 3) / Room Stability Move  (%d)\n", _LM3_RSM);
		fprintf(stderr, "\t(Lecture Move 3 + Lecture Move 4) / Kempe estendido com restrição de sala (%d)\n", _LM3_LM4_KERS);
		fprintf(stderr, "\t(Lecture Move 3 + Lecture Move 4) / Minimum Working Days Move  (%d)\n", _LM3_LM4_MWD);
		fprintf(stderr, "\t(Lecture Move 3 + Lecture Move 4) / Room Stability Move  (%d)\n", _LM3_LM4_RSM);
		exit(0);
	}

	return bestInd;
}
