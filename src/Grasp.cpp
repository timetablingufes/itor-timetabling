/*
 * Grasp.cpp
 *
 *  Created on: May 17, 2015
 *      Author: erika
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "Grasp.h"
#include "GeraSolucacoInicial.h"
#include "GeraSolucaoInicialGulosa.h"
#include "GeraSolucaoInicialGulosaAleatoria.h"
#include "Model/Problema.h"
#include "BuscaLocal/HillClimbing.h"
#include "BuscaLocal/SimulatedAnnealing.h"
#include "BuscaLocal/SteepestDescent.h"
#include "BuscaLocal/SDConjugado.h"
#include "BuscaLocal/BuscaTeste.h"

Grasp::Grasp() {
	pesoHard = 10000;
	vizinhancasUtilizadas = 0; //move 50% + swap 50%

	//Construcao GRASP
	threshold = 0.10;
	tipoConstrucaoInicial = 0;	// 0 = normal; 1 = gulosa; 2 = teste com os 2
	//Iterações GRASP
	nMaxIter = 9999;
	//HC
	nMaxIterSemMelhora = 10000;
	nMaxIterBuscaLocal = 999999;
	k = 10;
	//SA
	temperaturaInicial = 7.6;
	temperaturaFinal = 0.005;
	taxaDecaimentoTemperatura = 0.999;
	aceitaPioraSA = 1;

	mediaSolucoes = 0;
	soft1 = soft2 = soft3 = soft4 = 0;
	f1 = f2 = f3 = 0;
	opBuscaLocalGrasp = 2;	//busca padrao = HC
	info = 0;
	infoSD = 0;
	imprimeGrafico = 0;
	imprimeFO = 0;
	iteracoesExecutadas = 0;
	tempoExecucao = 0;
	tempoLimite = 999999999;
	seed = time(NULL);
	nomeArquivoInstanciaInicial[0] = '\0';
	nMovimentosRealizados = 0;

	nMoves = nSwaps = nKempes = nTimeMoves = nRoomMoves = nLectureMoves = nRoomStabilityMoves = nMinWorkingDaysMoves = nCurriculumCompactnessMoves = 0;
	nMovesValidos = nSwapsValidos = nKempesValidos = nTimeMovesValidos =
			nRoomMovesValidos = nLectureMovesValidos = nRoomStabilityMovesValidos = nMinWorkingDaysMovesValidos = nCurriculumCompactnessMovesValidos = 0;
	nMovesMelhora = nSwapsMelhora = nKempesMelhora = nTimeMovesMelhora =
			nRoomMovesMelhora = nLectureMovesMelhora = nRoomStabilityMovesMelhora = nMinWorkingDaysMovesMelhora = nCurriculumCompactnessMovesMelhora = 0;
	nMovesInvalido = nSwapsInvalido = nKempesInvalido = nTimeMovesInvalido =
			nRoomMovesInvalido = nLectureMovesInvalido = nRoomStabilityMovesInvalido = nMinWorkingDaysMovesInvalido = nCurriculumCompactnessMovesInvalido = 0;
}

Individuo* Grasp::executa(Problema* p) {
	int i, nTentativas ;	
	//int fezPR;
	Individuo *ind, *bestInd = NULL, /**indPr,*/*indConstrucao;
	GeraSolucacoInicial* geraSolucaoI;
	GeraSolucaoInicialGulosa* geraSolucaoG;
	GeraSolucaoInicialGulosaAleatoria* geraSolucaoA;
	BuscaLocal* buscalocal;
	float tempo_inicial, tempo_construcao, tempo_busca_local;

	/*if (info > 1 && opBuscaLocalGrasp == 1) {
		infoSD = 1;
		info = 0;
		printf("\n%d\n", seed);
	}*/
	srand(seed);

	//pathrelinking = new PathRelinking();

	tempoExecucao = clock() / CLOCKS_PER_SEC;

	for (i = 0; i < nMaxIter; i++) {

		tempo_inicial = clock() / CLOCKS_PER_SEC;
		//seed = i * 100;
		//srand(seed);
		
		//CONSTRUCAO
		indConstrucao = new Individuo(p);
		if (i == 0 && nomeArquivoInstanciaInicial[0] != '\0') {
			if (info)
				printf("\tLe Solucao Inicial do arquivo '%s'\n",
						nomeArquivoInstanciaInicial);
			indConstrucao->LeDoArquivo(nomeArquivoInstanciaInicial);
		} else {
			if (tipoConstrucaoInicial == 0) {
				geraSolucaoI = new GeraSolucacoInicial(p, threshold);
				nTentativas = 1;
				//fprintf(stderr, "geraSolucaoI->CriaIndividuoInicial\n");
				while (geraSolucaoI->CriaIndividuoInicial(p, indConstrucao)){
					delete(indConstrucao);
					delete (geraSolucaoI);
					geraSolucaoI = new GeraSolucacoInicial(p, threshold);
					indConstrucao = new Individuo(p);
					nTentativas++;
				}
				delete (geraSolucaoI);
			} else if (tipoConstrucaoInicial == 1) {
				geraSolucaoG = new GeraSolucaoInicialGulosa(p, threshold);
				geraSolucaoG->CriaIndividuoInicial(p, indConstrucao);
				delete (geraSolucaoG);
			} else if (tipoConstrucaoInicial == 2) {
				geraSolucaoA = new GeraSolucaoInicialGulosaAleatoria(p, threshold);
				nTentativas = 1;
				while (geraSolucaoA->CriaIndividuoInicial(p, indConstrucao)){
					delete(indConstrucao);
					delete (geraSolucaoA);
					geraSolucaoA = new GeraSolucaoInicialGulosaAleatoria(p, threshold);
					indConstrucao = new Individuo(p);
					nTentativas++;
				}				
				delete (geraSolucaoA);
			}
		}

		//printf("Calcula Funcao Objetivo\n");
		p->CalculaFuncaoObjetivo(indConstrucao);
		f1 += indConstrucao->fitness;
		//fprintf(stderr, "%4d %4d %4d %4d %4d      => ", indConstrucao->hard, indConstrucao->soft1, indConstrucao->soft2, indConstrucao->soft3, indConstrucao->soft4);
		
		if (indConstrucao->hard > 0) {
			fprintf(stderr, "ERRO Construcao Grasp: Restricao Forte Conflitante\n");
			fprintf(stderr, "fo: %5d\n", indConstrucao->fitness);
			fprintf(stderr, "%4d %4d %4d %4d %4d\n", indConstrucao->hard, indConstrucao->soft1, indConstrucao->soft2, indConstrucao->soft3, indConstrucao->soft4);
			return indConstrucao;
		}

		tempo_construcao = clock() / CLOCKS_PER_SEC;
		if (imprimeGrafico){
			printf("%3d;", seed);
			printf("%6d;%6d;%6d;%6.2f;", i+1, nTentativas, indConstrucao->fitness, tempo_construcao - tempo_inicial);
		}
		if (info) {
			printf("Construção %3d: %5d em %4.2f s\n", i, indConstrucao->fitness, tempo_construcao - tempo_inicial);
		}
		if (infoSD) printf("%d\n", indConstrucao->fitness);


		//BUSCA LOCAL
		//printf("Executa buscalocal\n");
		if (opBuscaLocalGrasp == 2) {
			ind = executaHillClimbing(p, indConstrucao, &buscalocal);
		} else if (opBuscaLocalGrasp == 6) {
			ind = executaSimulatedAnnealing(p, indConstrucao, &buscalocal);
		} else if (opBuscaLocalGrasp == 3) {
			ind = executaHillClimbing(p, indConstrucao, &buscalocal);
			atualizaEstatisticasBuscaLocal(buscalocal);
			delete (buscalocal);
			ind = executaSimulatedAnnealing(p, ind, &buscalocal);
		} else if (opBuscaLocalGrasp == 1) {
			ind = executaSteepestDescent(p, indConstrucao, &buscalocal);
		} else if (opBuscaLocalGrasp == 5) {
			ind = executaBuscaTeste(p, indConstrucao, &buscalocal);
		} else if (opBuscaLocalGrasp == 4) {
			buscalocal = NULL;
			ind = indConstrucao;
		} else {
			fprintf(stderr, "Tipo de busca local nao foi informada!\n");
			exit(1);
		}

		f2 += ind->fitness;
		if (ind->fitness >= 100000) {
			fprintf(stderr, "ERRO Busca local: Restricao Forte Conflitantes: %d\n", ind->hard);
			exit(0);
		}
		//fprintf(stderr, "%4d %4d %4d %4d %4d\n", ind->hard, ind->soft1, ind->soft2, ind->soft3, ind->soft4);

		//p->CalculaFuncaoObjetivo(ind);
		atualizaEstatisticasBuscaLocal(buscalocal);
		soft1 += ind->soft1;
		soft2 += ind->soft2;
		soft3 += ind->soft3;
		soft4 += ind->soft4;

		tempo_busca_local = (clock() / CLOCKS_PER_SEC);
		if (imprimeGrafico){
			printf("%5d;%6.2f;", ind->fitness, tempo_busca_local - tempo_construcao);
		}
		if (info) {
			printf("Busca Local   : %5d em %4.2f s\n", ind->fitness, tempo_busca_local - tempo_construcao);
		}
		if (imprimeGrafico){
			printf("%7d;", buscalocal->nIteracoesExecutadas);
			printf("%10d;", buscalocal->nMovimentosRealizados);
			printf("%10d;", (buscalocal->nMovesValidos+
							 buscalocal->nSwapsValidos+
							 buscalocal->nKempesValidos+
							 buscalocal->nTimeMovesValidos+
							 buscalocal->nRoomMovesValidos+
							 buscalocal->nLectureMovesValidos+
							 buscalocal->nRoomStabilityMovesValidos+
							 buscalocal->nMinWorkingDaysMovesValidos+
							 buscalocal->nCurriculumCompactnessMovesValidos
							 ) 
					);
			printf("%10d;", (buscalocal->nMovesMelhora+
							 buscalocal->nSwapsMelhora+
							 buscalocal->nKempesMelhora+
							 buscalocal->nTimeMovesMelhora+
							 buscalocal->nRoomMovesMelhora+
							 buscalocal->nLectureMovesMelhora+
							 buscalocal->nRoomStabilityMovesMelhora+
							 buscalocal->nMinWorkingDaysMovesMelhora+
							 buscalocal->nCurriculumCompactnessMovesMelhora
							 ) 
					);
			printf("%10d;", (buscalocal->nMovesInvalido+
							 buscalocal->nSwapsInvalido+
							 buscalocal->nKempesInvalido+
							 buscalocal->nTimeMovesInvalido+
							 buscalocal->nRoomMovesInvalido+
							 buscalocal->nLectureMovesInvalido+
							 buscalocal->nRoomStabilityMovesInvalido+
							 buscalocal->nMinWorkingDaysMovesInvalido+
							 buscalocal->nCurriculumCompactnessMovesInvalido
							 ) 
					);
			printf("%10d;", (buscalocal->nMoves+buscalocal->nSwaps+buscalocal->nKempes+buscalocal->nTimeMoves+
							 buscalocal->nRoomMoves+buscalocal->nLectureMoves+buscalocal->nRoomStabilityMoves+
							 buscalocal->nMinWorkingDaysMoves+buscalocal->nCurriculumCompactnessMoves) );
			printf("%10d;", buscalocal->nMoves+buscalocal->nLectureMoves);
			printf("%10d;", buscalocal->nSwaps);
			printf("%10d;", buscalocal->nKempes+buscalocal->nRoomStabilityMoves+buscalocal->nMinWorkingDaysMoves);
			//printf("%10d;%10d;%10d;", buscalocal->nTimeMoves, buscalocal->nRoomMoves, buscalocal->nLectureMoves);
			//printf("%10d;%10d;", buscalocal->nRoomStabilityMoves, buscalocal->nMinWorkingDaysMoves);
		}
		delete (buscalocal);

		f3 += ind->fitness;
		mediaSolucoes += ind->fitness;

		//DELETE
		if ((bestInd == NULL) || (ind->fitness < bestInd->fitness)) {
			if (bestInd != NULL)
				delete (bestInd);
			bestInd = ind;
		} else
			delete (ind);

		if (info) {
			printf("\n");
		}
		//FIM ITERACAO
		iteracoesExecutadas++;
		if (imprimeGrafico){
			printf("%6.2f;", (clock() / CLOCKS_PER_SEC) - tempo_inicial);
			printf("%3d;%6.0f\n", iteracoesExecutadas, (clock() / CLOCKS_PER_SEC) - tempoExecucao);
		}
		if (this->tempoLimite <= (clock() / CLOCKS_PER_SEC) - tempoExecucao)
			break;

	}
	tempoExecucao = (clock() / CLOCKS_PER_SEC) - tempoExecucao;

	if (info) {
		printf("Executou %d iteracoes em %f s\n", iteracoesExecutadas, tempoExecucao);
		printf("Num Movimentos Realizados: %d\n", nMovimentosRealizados);
	}
	if (infoSD) {
		printf("%f\n", tempoExecucao);
		printf("%d\n", nMovimentosRealizados);
	}

	//delete(pathrelinking);

	mediaSolucoes /= iteracoesExecutadas;

	soft1 /= iteracoesExecutadas;
	soft2 /= iteracoesExecutadas;
	soft3 /= iteracoesExecutadas;
	soft4 /= iteracoesExecutadas;

	f1 /= iteracoesExecutadas;
	f2 /= iteracoesExecutadas;
	f3 /= iteracoesExecutadas;

	return bestInd;
}

Individuo* Grasp::executaHillClimbing(Problema* p, Individuo* indInicial, BuscaLocal** bl) {
	BuscaLocal* buscalocal;
	HillClimbing* hc;
	Individuo* ind;

	hc = new HillClimbing(p, nMaxIterBuscaLocal, k);
	ind = hc->executa(indInicial, vizinhancasUtilizadas, nMaxIterSemMelhora);
	buscalocal = hc;
	*bl = buscalocal;
	return ind;
}

Individuo* Grasp::executaSteepestDescent(Problema* p, Individuo* indInicial, BuscaLocal** bl) {
	BuscaLocal* buscalocal;
	SDConjugado* sd;
	Individuo* ind;

	sd = new SDConjugado(p);
	ind = sd->executa(indInicial, vizinhancasUtilizadas);
	buscalocal = sd;
	*bl = buscalocal;
	return ind;
}

Individuo* Grasp::executaSimulatedAnnealing(Problema* p, Individuo* indInicial, BuscaLocal** bl) {
	BuscaLocal* buscalocal;
	SimulatedAnnealing* sa;
	Individuo* ind;

	sa = new SimulatedAnnealing(p, nMaxIterSemMelhora, temperaturaInicial, temperaturaFinal, taxaDecaimentoTemperatura);
	if (vizinhancasUtilizadas == 100){
		//const int _LECTURE3  = 10;
		//const int _KEMPEEXTRESTSALA = 15;
		ind = sa->executa(indInicial, 10, info);
		ind = sa->executa(ind, 15, info);
	}
	else ind = sa->executa(indInicial, vizinhancasUtilizadas, info);
	buscalocal = sa;
	*bl = buscalocal;
	return ind;
}

Individuo* Grasp::executaBuscaTeste(Problema* p, Individuo* indInicial, BuscaLocal** bl) {
	BuscaLocal* buscalocal;
	BuscaTeste* sd;
	Individuo* ind;

	sd = new BuscaTeste(p);
	ind = sd->executa(indInicial, vizinhancasUtilizadas);
	buscalocal = sd;
	*bl = buscalocal;
	return ind;
}

void Grasp::atualizaEstatisticasBuscaLocal(BuscaLocal* buscalocal){
	if( buscalocal != NULL ){
		nMovimentosRealizados 		+= buscalocal->nMovimentosRealizados;
		nMoves 						+= buscalocal->nMoves;
		nSwaps 						+= buscalocal->nSwaps;
		nKempes 					+= buscalocal->nKempes;
		nTimeMoves 					+= buscalocal->nTimeMoves;
		nRoomMoves 					+= buscalocal->nRoomMoves;
		nLectureMoves 				+= buscalocal->nLectureMoves;
		nRoomStabilityMoves 		+= buscalocal->nRoomStabilityMoves;
		nMinWorkingDaysMoves 		+= buscalocal->nMinWorkingDaysMoves;
		nCurriculumCompactnessMoves += buscalocal->nCurriculumCompactnessMoves;
		nMovesMelhora 				+= buscalocal->nMovesMelhora;
		nSwapsMelhora 				+= buscalocal->nSwapsMelhora;
		nKempesMelhora 				+= buscalocal->nKempesMelhora;
		nTimeMovesMelhora 			+= buscalocal->nTimeMovesMelhora;
		nRoomMovesMelhora 			+= buscalocal->nRoomMovesMelhora;
		nLectureMovesMelhora 		+= buscalocal->nLectureMovesMelhora;
		nRoomStabilityMovesMelhora 	+= buscalocal->nRoomStabilityMovesMelhora;
		nMinWorkingDaysMovesMelhora += buscalocal->nMinWorkingDaysMovesMelhora;
		nCurriculumCompactnessMovesMelhora += buscalocal->nCurriculumCompactnessMovesMelhora;
		nMovesValidos 				+= buscalocal->nMovesValidos;
		nSwapsValidos 				+= buscalocal->nSwapsValidos;
		nKempesValidos 				+= buscalocal->nKempesValidos;
		nTimeMovesValidos 			+= buscalocal->nTimeMovesValidos;
		nRoomMovesValidos 			+= buscalocal->nRoomMovesValidos;
		nLectureMovesValidos 		+= buscalocal->nLectureMovesValidos;
		nRoomStabilityMovesValidos 	+= buscalocal->nRoomStabilityMovesValidos;
		nMinWorkingDaysMovesValidos += buscalocal->nMinWorkingDaysMovesValidos;
		nCurriculumCompactnessMovesValidos += buscalocal->nCurriculumCompactnessMovesValidos;
		nMovesInvalido 				+= buscalocal->nMovesInvalido;
		nSwapsInvalido 				+= buscalocal->nSwapsInvalido;
		nKempesInvalido 			+= buscalocal->nKempesInvalido;
		nTimeMovesInvalido 			+= buscalocal->nTimeMovesInvalido;
		nRoomMovesInvalido 			+= buscalocal->nRoomMovesInvalido;
		nLectureMovesInvalido 		+= buscalocal->nLectureMovesInvalido;
		nRoomStabilityMovesInvalido 	+= buscalocal->nRoomStabilityMovesInvalido;
		nMinWorkingDaysMovesInvalido 	+= buscalocal->nMinWorkingDaysMovesInvalido;
		nCurriculumCompactnessMovesInvalido += buscalocal->nCurriculumCompactnessMovesInvalido;
	}

}

void Grasp::ImprimeExecucao() {
	printf("\nSeed:  %d\n", seed);
	printf("Numero maximo de iteracoes:  %d\n", nMaxIter);

	if (opBuscaLocalGrasp == 2) {
		printf("Busca Local: HC\n");
		printf("Maximo de Iteracoes Busca Local sem Melhora: %d\n", nMaxIterSemMelhora);
		printf("Maximo de Iteracoes Busca Local Geral: %d\n", nMaxIterBuscaLocal);
		printf("k:  %d\n", k);
	} else if (opBuscaLocalGrasp == 6) {
		printf("Busca Local: SA\n");
		printf("Variacao de temperatura:  %lf a %lf, a uma taxa de %lf\n", temperaturaInicial, temperaturaFinal, taxaDecaimentoTemperatura);
		printf("N:  500\n");
		printf("Na: %3d\n", nMaxIterSemMelhora);
	} else if (opBuscaLocalGrasp == 1) {
		printf("Busca Local: SD\n");
	} else if (opBuscaLocalGrasp == 5) {
		printf("Busca Local: Teste\n");
	} else
		printf("Busca Local Invalida (%d)\n", opBuscaLocalGrasp);
	printf("Opcao de Vizinhanca:  %d\n", vizinhancasUtilizadas);
	printf("\nNumero de Movimentos Realizados: %d\n", nMovimentosRealizados);
	if( nMovimentosRealizados > 100 ){
	if (nMoves >= 100000 || nSwaps >= 100000) {
		printf(
				"Total de Movimentos   :  nMv: %6d nSw: %6d nK: %6d nTimeMv: %4d nRoomMv: %4d nLectMv: %4d nRoomStbMv: %4d nMinWkMv: %4d nCurCompMv: %4d\n",
				nMoves, nSwaps, nKempes, nTimeMoves, nRoomMoves, nLectureMoves,
				nRoomStabilityMoves, nMinWorkingDaysMoves,
				nCurriculumCompactnessMoves);
		printf(
				"Movimentos Invalidos  :  nMv: %6d nSw: %6d nK: %6d nTimeMv: %4d nRoomMv: %4d nLectMv: %4d nRoomStbMv: %4d nMinWkMv: %4d nCurCompMv: %4d\n",
				nMovesInvalido, nSwapsInvalido, nKempesInvalido,
				nTimeMovesInvalido, nRoomMovesInvalido, nLectureMovesInvalido,
				nRoomStabilityMovesInvalido, nMinWorkingDaysMovesInvalido,
				nCurriculumCompactnessMovesInvalido);
		printf(
				"Movimentos Validos    :  nMv: %6d nSw: %6d nK: %6d nTimeMv: %4d nRoomMv: %4d nLectMv: %4d nRoomStbMv: %4d nMinWkMv: %4d nCurCompMv: %4d\n",
				nMovesValidos, nSwapsValidos, nKempesValidos, nTimeMovesValidos,
				nRoomMovesValidos, nLectureMovesValidos,
				nRoomStabilityMovesValidos, nMinWorkingDaysMovesValidos,
				nCurriculumCompactnessMovesValidos);
		printf(
				"Movimentos com Melhora:  nMv: %6d nSw: %6d nK: %6d nTimeMv: %4d nRoomMv: %4d nLectMv: %4d nRoomStbMv: %4d nMinWkMv: %4d nCurCompMv: %4d",
				nMovesMelhora, nSwapsMelhora, nKempesMelhora, nTimeMovesMelhora,
				nRoomMovesMelhora, nLectureMovesMelhora,
				nRoomStabilityMovesMelhora, nMinWorkingDaysMovesMelhora,
				nCurriculumCompactnessMovesMelhora);
	} else {
		printf(
				"Total de Movimentos   :  nMv: %5d nSw: %5d nK: %5d nTimeMv: %5d nRoomMv: %5d nLectMv: %5d nRoomStbMv: %5d nMinWkMv: %5d nCurCompMv: %5d\n",
				nMoves, nSwaps, nKempes, nTimeMoves, nRoomMoves, nLectureMoves,
				nRoomStabilityMoves, nMinWorkingDaysMoves,
				nCurriculumCompactnessMoves);
		printf(
				"Movimentos Invalidos  :  nMv: %5d nSw: %5d nK: %5d nTimeMv: %5d nRoomMv: %5d nLectMv: %5d nRoomStbMv: %5d nMinWkMv: %5d nCurCompMv: %5d\n",
				nMovesInvalido, nSwapsInvalido, nKempesInvalido,
				nTimeMovesInvalido, nRoomMovesInvalido, nLectureMovesInvalido,
				nRoomStabilityMovesInvalido, nMinWorkingDaysMovesInvalido,
				nCurriculumCompactnessMovesInvalido);
		printf(
				"Movimentos Validos    :  nMv: %5d nSw: %5d nK: %5d nTimeMv: %5d nRoomMv: %5d nLectMv: %5d nRoomStbMv: %5d nMinWkMv: %5d nCurCompMv: %5d\n",
				nMovesValidos, nSwapsValidos, nKempesValidos, nTimeMovesValidos,
				nRoomMovesValidos, nLectureMovesValidos,
				nRoomStabilityMovesValidos, nMinWorkingDaysMovesValidos,
				nCurriculumCompactnessMovesValidos);
		printf(
				"Movimentos com Melhora:  nMv: %5d nSw: %5d nK: %5d nTimeMv: %5d nRoomMv: %5d nLectMv: %5d nRoomStbMv: %5d nMinWkMv: %5d nCurCompMv: %5d",
				nMovesMelhora, nSwapsMelhora, nKempesMelhora, nTimeMovesMelhora,
				nRoomMovesMelhora, nLectureMovesMelhora,
				nRoomStabilityMovesMelhora, nMinWorkingDaysMovesMelhora,
				nCurriculumCompactnessMovesMelhora);
	}
	}

	printf("\nIteracoes do GRASP executadas: %d em %lf s\n",
			iteracoesExecutadas, tempoExecucao);
	printf("Media das solucoes:  %d\n", mediaSolucoes);
//	printf("\tSoft1:  %d\tSoft2:  %d\tSoft3:  %d\tSoft4:  %d\n", soft1, soft2, soft3, soft4);
//	printf("f1:  %d\tf2:  %d\tf3:  %d\t\n", f1, f2, f3);
}

void Grasp::ImprimeExecucaoResumida() {

	if (nMoves > 0) {
		printf("Move\n");
		printf("%6d\n", nMoves);
		printf("%6d\n", nMovesInvalido);
		printf("%6d\n", nMovesValidos);
		printf("%6d\n", nMovesMelhora);
	}
	if (nSwaps > 0) {
		printf("Swap\n");
		printf("%6d\n", nSwaps);
		printf("%6d\n", nSwapsInvalido);
		printf("%6d\n", nSwapsValidos);
		printf("%6d\n", nSwapsMelhora);
	}
	if (nKempes > 0) {
		printf("Kempe\n");
		printf("%6d\n", nKempes);
		printf("%6d\n", nKempesInvalido);
		printf("%6d\n", nKempesValidos);
		printf("%6d\n", nKempesMelhora);
	}

}
