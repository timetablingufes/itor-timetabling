#!/bin/bash
reset

#Informações colhidas nos testes:
#Seed
#fo da construção
#tempo da construção
#fo da busca local
#tempo da busca local
#número de iteracoes da busca local
#número de movimentos realizados
#número de movimentos tentados
#número de movimentos tentados de cada tipo
#número total de iterações GRASP
#tempo total


cd src
mkdir -p bin
make
cd ../src

mkdir -p ../results
mkdir -p ../tests
mkdir -p ../tests/summaryperinstance

testes=( GRASP_C0_SA_LM3 )
#seeds=( 2 7 11 13 17 19 23 29 31 37 ) 
seeds=( 41 43 47 53 59 61 67 71 73 79 )
#seeds=( 83 89 97 101 103 107 109 113 127 131 137 139 149 151 157 163 167 173 179 181 191 193 197 199 211 223 227 229 )
instances=( Udine1 Udine2 Udine3 Udine4 Udine5 Udine6 Udine7 Udine8 Udine9 UUMCAS_A131 )
instances=( DDS2 DDS3 DDS4 DDS5 DDS6 DDS7 )


for teste in ${testes[@]} ; do
	echo "testing $teste..."
	#Cria o diretório onde vão ficar os resultados
	mkdir -p ../tests/$teste

	#Executa as instâncias para todas as seeds
	for instance in ${instances[@]} ; do
			echo "Instancia $instance"
			instancia=../instances/$instance.ctt

			for seed in ${seeds[@]} ; do
				echo "Seed $seed"
				resultado=../results/$teste.$instance.$seed.res
				saida=../tests/$teste/$instance-$seed.dat

				if [ $teste = GRASP_C0_SA_LM3 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=220 const=0 grafico=1 t0=105.3 tFinal=0.005 beta=0.9999 > $saida
				elif [ $teste = GRASP_C0_SA_LM3_KERS10 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=23 timeout=220 const=0 grafico=1 t0=17.3 tFinal=0.003 beta=0.9999 > $saida
				elif [ $teste = GRASP_C0_SA_LM3_KERS20 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=24 timeout=220 const=0 grafico=1 t0=17.3 tFinal=0.003 beta=0.9999 > $saida
				elif [ $teste = GRASP_C0_SA_LM3_KERS30 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=25 timeout=220 const=0 grafico=1 t0=17.3 tFinal=0.003 beta=0.9999 > $saida
				elif [ $teste = GRASP_C0_SA_LM3_SETA_KERS ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=100 timeout=220 const=0 grafico=1 t0=17.3 tFinal=0.003 beta=0.9999 > $saida
				else
				  echo "Invalid test ($teste)"
				fi

			done
			cat ../tests/$teste/$instance-* > ../tests/summaryperinstance/$teste-$instance.dat
	done

done

echo "done"
