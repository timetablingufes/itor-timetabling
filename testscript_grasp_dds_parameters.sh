#!/bin/bash
reset

#GraspParameters (Const=C2  viz=LM3)
testes=( __1 __2 __3 __4 __5 __6 __7 __8 __9 __10 )
seeds=( 2 3 5 7 11 13 17 19 )

#Informações colhidas nos testes:
#Seed
#fo da construção
#tempo da construção
#fo da busca local
#tempo da busca local
#número de iteracoes da busca local
#número de movimentos realizados
#número de movimentos tentados
#número de movimentos tentados de cada tipo
#número total de iterações GRASP
#tempo total


cd src
mkdir -p bin
make
cd ../src

mkdir -p ../results
mkdir -p ../tests
mkdir -p ../tests/summaryperinstance

# Rodar erlangen
testes=( __1 __2 __3 __4 __5 __6 __7 __8 __9 __10 )
seeds=( 2 3 5 7 11 13 17 19 )
instances=( erlangen2011_2 erlangen2012_1 erlangen2012_2 erlangen2013_1 erlangen2013_2 erlangen2014_1 )

# Rodar Udine e UUMCAS_A131
testes=( __11 __12 )
#__7 __8 __9 __10 )
seeds=( 2 3 5 7 11 )
#13 17 19 23 27 )
instances=( Udine1 Udine2 Udine3 Udine4 Udine5 Udine6 Udine7 Udine8 Udine9 UUMCAS_A131 )


# instances=( DDS1 DDS2 DDS3 DDS4 DDS5 DDS6 DDS7 )
#instances=( DDS1 DDS4 EA01 EA06 EA12 erlangen2011_2 erlangen2012_1 Udine2 Udine8 UUMCAS_A131 )
#instances=( UUMCAS_A131 )
#instances=( EA01 EA02 EA03 EA04 EA05 EA06 EA07 EA08 EA09 EA10 EA11 EA12 )

#seeds=( 2 )
#testes=( __0 )
#instances=( Udine1 Udine2 Udine3 Udine4 Udine5 Udine6 Udine7 Udine8 Udine9 erlangen2011_2 erlangen2012_1 erlangen2012_2 erlangen2013_1 erlangen2013_2 erlangen2014_1 UUMCAS_A131 )

title="$instance"
for teste in ${testes[@]} ; do
	echo "testing $teste..."
	#Cria o diretório onde vão ficar os resultados
	mkdir -p ../tests/$teste

	#Executa as instâncias para todas as seeds
	for instance in ${instances[@]} ; do
		  echo "Instancia $instance"
			instancia=../instances/$instance.ctt

			for seed in ${seeds[@]} ; do
				echo "Seed $seed"
				resultado=../results/$teste.$instance.$seed.res
				saida=../tests/$teste/$instance-$seed.dat

				#Testing Contruction Algoritms
				if [ $teste = __0 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=20 const=0 grafico=1 t0=7.6  tFinal=0.005 beta=0.9999 > $saida
				elif [ $teste = __1 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=220 const=0 grafico=1 t0=7.6  tFinal=0.005 beta=0.9999 > $saida
				elif [ $teste = __2 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=220 const=0 grafico=1 t0=21.3  tFinal=0.005 beta=0.9999 > $saida
				elif [ $teste = __3 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=220 const=0 grafico=1 t0=21.3  tFinal=0.005 beta=0.9998 > $saida
				elif [ $teste = __4 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=220 const=0 grafico=1 t0=28.9  tFinal=0.005 beta=0.9999 > $saida
				elif [ $teste = __5 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=220 const=0 grafico=1 t0=28.9  tFinal=0.005 beta=0.9998 > $saida
				elif [ $teste = __6 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=220 const=0 grafico=1 t0=31.6 tFinal=0.005 beta=0.9999 > $saida
				elif [ $teste = __7 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=220 const=0 grafico=1 t0=40.8 tFinal=0.005 beta=0.9998 > $saida
				elif [ $teste = __8 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=220 const=0 grafico=1 t0=40.8 tFinal=0.005 beta=0.9999 > $saida
				elif [ $teste = __9 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=220 const=0 grafico=1 t0=45.3 tFinal=0.005 beta=0.9998 > $saida
				elif [ $teste = __10 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=220 const=0 grafico=1 t0=105.3 tFinal=0.005 beta=0.9980 > $saida
				elif [ $teste = __11 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=220 const=0 grafico=1 t0=45.3 tFinal=0.005 beta=0.9999 > $saida
				elif [ $teste = __12 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=220 const=0 grafico=1 t0=105.3 tFinal=0.005 beta=0.9999 > $saida
				elif [ $teste = __12 ]; then
					./grasp $instancia $resultado seed=$seed bl=sa viz=10 timeout=220 const=0 grafico=1 t0=105.3 tFinal=0.005 beta=0.9999 > $saida
				else
				  echo "Invalid test ($teste)"
				fi

			done
			cat ../tests/$teste/$instance-* > ../tests/summaryperinstance/$teste-$instance.dat
	done
done

echo "done"
